import time
from adafruit_servokit import ServoKit


nbP=2

min_imp=[500,500]
max_imp=[2500,2500]
min_ang=[0,0]
max_ang=[180,180]


pca=ServoKit(channels=16, address=0x40)

def init():

	for i in range(nbP):
		pca.servo[i+1].set_pulse_width_range(min_imp[i],max_imp[i])

def main():
	init()
	ServoInit()
#	pcaScenario()

def ServoInit():
	pca.servo[1].angle=90
	pca.servo[2].angle=90

def pcaScenario():

	for i in range(nbP):
		for j in range(min_ang[i],max_ang[i],1):
			print("angle {} to servo {}".format(j,i))
			pca.servo[i+1].angle=j
			time.sleep(0.01)
		for j in range (max_ang[i],min_ang[i],-1):
			print("angle {} to servo {}".format(j,i))
			pca.servo[i+1].angle=j
			time.sleep(0.01)
		pca.servo[i].angle=None
		time.sleep(0.5)

main()
