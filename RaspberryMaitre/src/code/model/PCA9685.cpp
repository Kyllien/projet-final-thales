#include <PCA9685.hpp>

PCA9685::PCA9685(uint8_t i2c_bus, uint8_t i2c_address) 
{
    deviceI2C = new i2c(i2c_bus, i2c_address);
}

PCA9685::~PCA9685(){
    if(deviceI2C != NULL)
        delete deviceI2C;
}

bool PCA9685::initialize(){
    deviceI2C->WriteReg8(MODE2, OUTDRV);
    deviceI2C->WriteReg8(MODE1, ALLCALL);
    //time.sleep(0.005)  # wait for oscillator
    usleep(5000);
    uint8_t mode1 = deviceI2C->ReadReg8(MODE1);
    mode1 = mode1 & ~SLEEP;  // wake up (reset sleep)
    deviceI2C->WriteReg8(MODE1, mode1);
    //time.sleep(0.005)  # wait for oscillator
    usleep(5000);
    setPwmFrequency(50);
    PLOGI<<"PCA9685 Initialization: SUCCESS";
    
    return true;
}

void PCA9685::setPwmFrequency(uint16_t frequency){
    float prescaleval = 25000000.0; //25MHz
    prescaleval /= 4096.0;         //12-bit
    prescaleval /= float(frequency);
    prescaleval -= 1.0;
    PLOGI <<"Setting PWM frequency to "<<frequency<<" hz";
    PLOGI <<"Estimated pre-scale: "<<prescaleval;
    int prescale = (int)floor(prescaleval + 0.5);
    PLOGI <<"Final pre-scale: "<<prescale;
    uint8_t oldmode = deviceI2C->ReadReg8(MODE1);
    uint8_t newmode = (oldmode & 0x7F) | 0x10; // sleep
    deviceI2C->WriteReg8(MODE1, newmode);      //go to sleep
    deviceI2C->WriteReg8(PRESCALE, prescale);
    deviceI2C->WriteReg8(MODE1, oldmode);
    //time.sleep(0.005)  # wait
    usleep(5000);
    deviceI2C->WriteReg8(MODE1, oldmode | 0x80);
}

void PCA9685::setPwm(uint8_t channel, uint16_t on, uint16_t off){
    deviceI2C->WriteReg8(LED0_ON_L+4*channel, on & 0xFF);
    deviceI2C->WriteReg8(LED0_ON_H+4*channel, on >> 8);
    deviceI2C->WriteReg8(LED0_OFF_L+4*channel, off & 0xFF);
    deviceI2C->WriteReg8(LED0_OFF_H+4*channel, off >> 8);
}

void PCA9685::setAllPwm(uint16_t on, uint16_t off){
    deviceI2C->WriteReg8(ALL_LED_ON_L, on & 0xFF);
    deviceI2C->WriteReg8(ALL_LED_ON_H, on >> 8);
    deviceI2C->WriteReg8(ALL_LED_OFF_L, off & 0xFF);
    deviceI2C->WriteReg8(ALL_LED_OFF_H, off >> 8);
}

void PCA9685::move(uint8_t channel, int deg){
    float pwm = 500.0 + ((deg/180.0) * 2000.0);
    pwm = (4096.0/20000.0) * pwm;
    setPwm(channel, 0, (int) pwm);
}

bool PCA9685::ServoMoteurs_init()
{
    if(!initialize()){
        PLOGE << "PCA9685 device init failed";
        return false;
    }
    move(1,0);
    move(2,0);
    PLOGI << "Servos initialized";
    return true;
}

void PCA9685::Blue_servo_scenario_json()
{
    PLOGI << "Start Blue Scenario json";
    move(2,135);
    usleep(1000000*0.5);
    move(2,45);
    usleep(1000000*0.5);
    move(2,135);
    usleep(1000000*0.5);
    move(2,180);
    PLOGI << "End Blue Scenario TCP";
}

void PCA9685::Blue_servo_scenario_tcp()
{
    PLOGI << "Start Blue Scenario TCP";
    move(2,90);
    usleep(1000000*0.5);
    move(2,180);
    PLOGI << "End Blue Scenario TCP";
}

void PCA9685::Yellow_servo_scenario(int &angle, bool &is_movement)
{ 
    if(is_movement && angle!=90) {
        PLOGI << "Movement detected";
        move(1,90);
        usleep(1000000*0.5);
        angle = 90;
    }
    else if(!is_movement && angle==90) {
        PLOGI << "Movement ended";
        move(1,180);
	      usleep(1000000*0.5);
		    angle = 180;
    } 
}

