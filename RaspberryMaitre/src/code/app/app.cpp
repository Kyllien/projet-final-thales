#include "app.hpp"


bool app::init(int argc, char* argv[])
{
    try
    {
        init_logs(argc >1 ? string(argv[1]): string(""));
        PLOGI << "Program Init";
        return true;
    }
    catch(const std::exception& e)
    {
        PLOGF << "Program init error:" << e.what();
        return false;
    }
}

void app::run()
{
    PLOGI << "Program starting";

    try
    {
        //-------------Thread--------------------------
        // Global Variables
        mutex mtx_Sample;
        mutex mtx_JSON;
        vector<string> data;
        json json_sample;
        //---------------------------------------------

        bool is_movement=false;
        bool is_sample=false;
        bool is_JSON=false;
        int data_rate_json;
        int data_rate_tcp;

        ofstream json_file("data/data.json");

        PCA9685 Servos;

        Servos.ServoMoteurs_init();
        Configuration(data_rate_json,data_rate_tcp);

        // Thread pour recuparer les donnaes provenant du TXRX et de l'inserer dans la variable data
        // Pour aviter tout conflit avec fonctionWriteJSON_thread le mutex mtx_Sample est utilisa pour lock data
        thread GetTxRx_thread([&mtx_Sample, &json_sample, &is_movement, &is_sample, &data_rate_json,  &data] () {

            UART Serial_receiver;
            string data_string;
            int timer = time(0);

            
            while (true) {
                this_thread::sleep_for(chrono::milliseconds(50));
                
                data_string="{}";

                Serial_receiver.reception(data_string);

                try{
                    json_sample= json::parse(data_string);
                    is_movement = json_sample.at("HC-SR501").at("Movement").get<int>() ;
                    
                    if(time(0)-timer >= data_rate_json) {
                        timer =time(0);
                        is_sample=true;
                        cout << "Sample received : " << endl << json_sample.dump();
                        unique_lock<mutex> lock_string(mtx_Sample); 
                            data.push_back(json_sample.dump());
                        lock_string.unlock();
                    }
                }  
                catch(const exception& e)
                {
                    PLOGF << "Invalid sample : " << e.what();
                }
                    

                }
            });

        // Thread pour gerer le servo moteur de detection de mouvement
        thread Yellow_Servo_thread([&is_movement, &Servos] () {
            int angle = 0;
            while(true){
                this_thread::sleep_for(chrono::milliseconds(50));
                  if(is_movement){
                    Servos.Yellow_servo_scenario(angle, is_movement);
                  }            
            }
        });

        // Thread pour gerer le servo moteur qui se deplace lorsque des donnees sont inscrites dans le fichier .json
        // Il a ete place en thread car le scanario met un certain temps qui pourrait influe sur les autres taches       
        thread Blue_Servo_thread([&Servos, &is_sample, &is_JSON](){ 
            while (true) {
                this_thread::sleep_for(chrono::milliseconds(50));
                if(is_sample) {
                    Servos.Blue_servo_scenario_json();
		                is_sample = false;
                }
                else if(is_JSON){
                    Servos.Blue_servo_scenario_tcp();
                    is_JSON = false;
                }
            }
        });    

        thread WriteJSON_thread([&mtx_Sample, &mtx_JSON, &data, &json_file,&data_rate_json] () {
            while(true)
            {
                this_thread::sleep_for(chrono::milliseconds(data_rate_json*1000));
                // COndition pour verifier que la variable qui contient les donnees provenant de la balise 
                // ne soit pas ouverte dans la fonction qui recupere ces donnees
                    
                    // Condition pour verifier que le fichier json n'est pas ouvert dans la fonction qui lis les donnees et les envois
                    unique_lock<mutex> lock_JSON(mtx_JSON);
                        if(!data.empty())
                        {
                          if(!json_file.is_open()){
                              json_file.open("data/data.json");
                          }
                        
                          unique_lock<mutex> lock_string(mtx_Sample); 
                          for(auto it : data){
                              json_file << it << endl;
                              PLOGI << "Sample saved in json file";
                          }
                          data.clear();
                          lock_string.unlock();
                        }
                    lock_JSON.unlock();
            }
        });

        // Thread qui permet de lire les donnaes dans le fichier .json et de le transmettre via TCP au serveur
        // Pour cela on utilise le mutex mtx_json pour verifier que le fichier n'est pas en ecriture
        // Lorsque cette tache est a effectue elle close json_file afin d'accader au .json et
        // pour l'ouvrir en input (ifstream)
        thread TCP_thread([&mtx_JSON, &json_file, &data_rate_tcp, &is_JSON] () {
            int sock;
            while (true) 
            {   
                this_thread::sleep_for(chrono::milliseconds(data_rate_tcp*1000));
                        // Initialisation Liaison TCP
                        sock = GestionTCP();
                        unique_lock<mutex> lock_JSON(mtx_JSON);
                        // Exception si socket/connection impossible a creer/etablir
                        if (sock!=0)
                        {
                            json_file.close();
                            // Initialisation Liaison TCP
                            PLOGI << "Opening data base";
                            JSONtoTCP(sock, &is_JSON);
                            PLOGI << "Closing data base";
                        }
                        lock_JSON.unlock();
            }  
        });

        GetTxRx_thread.join();
        WriteJSON_thread.join();
        TCP_thread.join();
        Blue_Servo_thread.join();
        Yellow_Servo_thread.join();
    }
        
        catch(const exception& e)
        {
            PLOGF << "Fatal error : " << e.what();
            PLOGI << "Restart program";
        }
}


bool app::Configuration(int &data_rate_json, int &data_rate_tcp)
{
    ifstream config_file(".data_log_master.conf");
        string conf;
        if (!config_file.is_open()) {
            PLOGE << "failed to open config file \".data_log_master.conf\"";
            return false;
        } 
        else {
            string tmp;
            while(config_file.peek() != EOF){
                getline(config_file, tmp) ;
                conf += tmp;
            }  
        }
        json config = json::parse(conf);
        config_file.close();

        data_rate_json = config.at("data_rate_json"); 
        data_rate_json = data_rate_json;
        data_rate_tcp = config.at("data_rate_tcp") ;
	      data_rate_tcp = data_rate_tcp;

        PLOGI << "Configuration success !";

        return true;
}


void app::clean()
{
    PLOGD << "Program cleaning";
}

void app::exit()
{
    PLOGW << "Program exiting";
}