/***************************************************************************
/*!
    \file     i2c.cpp
    \author   Philippe SIMIER (Touchard Wahington le Mans)
    \license  BSD (see license.txt)
    \brief    Classe pour le bus I2C
    \detail  I2C (pronounce: I squared C) is a protocol developed by Philips. It is a
	     slow two-wire protocol (variable speed, up to 400 kHz), with a high speed
	     extension (3.4 MHz).  It provides an inexpensive bus for connecting many
	     types of devices with in frequent or low bandwidth communications needs.
	     I2C is widely used with embedded systems.
    \version v1.0 - First release
*/

#include "i2c.hpp"

 // Le constructeur
i2c::i2c(int i2c_bus, int i2c_address)
    : i2c_bus(i2c_bus), i2c_address(i2c_address)
{
      i2cInit();
 }
 
i2c::~i2c()
{
}

void i2c::i2cInit()
{
    char filename[32];
    union i2c_smbus_data data ;

    snprintf(filename, 31, "/dev/i2c-%d", i2c_bus);
    if ((fd = open(filename, O_RDWR)) < 0){
        PLOGE <<"Failed to open i2c-" << i2c_bus << " connection";
        return;
    }
    if (ioctl(fd, I2C_SLAVE, i2c_address) < 0) {
        PLOGE <<"Failed to recognize device";
        return;
    }
    i2c_smbus_access (I2C_SMBUS_READ, 0, I2C_SMBUS_BYTE, &data);
    if(getError() != 0) {
        PLOGE << "Failed to request device";
        return;
	}
}


int i2c::getError(){
	return error;
 }

void i2c::i2c_smbus_access (char rw, uint8_t command, int size, union i2c_smbus_data *data)
{
    struct i2c_smbus_ioctl_data args ;

    args.read_write = rw ;
    args.command    = command ;
    args.size       = size ;
    args.data       = data ;

    errno=0;
    ioctl (fd, I2C_SMBUS, &args) ;
    error =  errno;
    if(error!=0) {PLOGE << strerror(errno);}
 }

unsigned char i2c::Read(){
    union i2c_smbus_data data ;

    i2c_smbus_access (I2C_SMBUS_READ, 0, I2C_SMBUS_BYTE, &data) ;
    if (getError() != 0) return 0;

    return data.byte & 0xFF ;
}

unsigned char i2c::ReadReg8 (int reg){
    union i2c_smbus_data data;

    i2c_smbus_access (I2C_SMBUS_READ, reg, I2C_SMBUS_BYTE_DATA, &data);
    if (getError() != 0) return 0;
    
    return data.byte & 0xFF ;
}

unsigned short i2c::ReadReg16 (int reg){
    union i2c_smbus_data data;

    i2c_smbus_access (I2C_SMBUS_READ, reg, I2C_SMBUS_WORD_DATA, &data);
    if (getError() != 0) return 0;
        
    return data.word & 0xFFFF ;
}

void i2c::Write (int data){
    i2c_smbus_access (I2C_SMBUS_WRITE, data, I2C_SMBUS_BYTE, NULL);
}

void i2c::WriteReg8 (int reg, int value){
    union i2c_smbus_data data ;
    data.byte = value ;
    i2c_smbus_access (I2C_SMBUS_WRITE, reg, I2C_SMBUS_BYTE_DATA, &data)  ;   
}

void i2c::WriteReg16 (int reg, int value){
    union i2c_smbus_data data ;
    data.word = value ;
    i2c_smbus_access (I2C_SMBUS_WRITE, reg, I2C_SMBUS_WORD_DATA, &data) ;
}

void i2c::WriteBlockData (int reg, int length, int *values){
    union i2c_smbus_data data ;
    int i;
    if (length > 32)
	    length = 32;
    for (i = 1; i <= length; i++)
	    data.block[i] = values[i-1];
	data.block[0] = length;
    i2c_smbus_access (I2C_SMBUS_WRITE, reg, I2C_SMBUS_I2C_BLOCK_BROKEN , &data) ;
}

int i2c::ReadBlockData (int reg, int length, int *values){
    union i2c_smbus_data data;
    int i;

    if (length > 32)
        length = 32;
    data.block[0] = length;
    i2c_smbus_access(I2C_SMBUS_READ, reg, length == 32 ? I2C_SMBUS_I2C_BLOCK_BROKEN : I2C_SMBUS_I2C_BLOCK_DATA,&data);
    if (getError() != 0) return 0;
    else {
        for (i = 1; i <= data.block[0]; i++)
            values[i-1] = data.block[i];
        return data.block[0];
    }
 }
 // retourne le nombre d'octets lu
