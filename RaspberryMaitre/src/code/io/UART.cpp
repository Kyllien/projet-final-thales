#include <UART.hpp>

int  wait_flag = FALSE;


void signal_handler_IO (int status)
{
//        printf("received SIGIO signal.\n");
        wait_flag = FALSE;
}


UART::~UART()
{
        close(serial_port);
}

UART::UART()
{
        init();
        interface(tty);
	PLOGI << "UART ready";
}

void UART::init()
{
        this->STOP=FALSE;
//        this->wait_flag=TRUE;

        this->serial_port = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NONBLOCK);

        if(tcgetattr(serial_port, &tty) != 0) {
//                printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
                PLOGE << "Serial tcgetattr error : " << strerror(errno);
        }

        // Save tty settings, also checking for error
        if (tcsetattr(serial_port, TCSANOW, &tty) != 0) {
//                printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
                PLOGE << "Serial tcsetattr error : " << strerror(errno);
        }

        // tcflush(serial_port, TCIFLUSH);

	signal(saio);
}


void UART::signal (struct sigaction saio)//!modifier l'action d'un processus à la reception d'un signal
{
            //!installation du signal handler
            saio.sa_handler = signal_handler_IO;
            sigemptyset(&(saio.sa_mask));
            saio.sa_flags = NULL;
            saio.sa_restorer = NULL;
            sigaction(SIGIO,&saio,NULL);//! fonction qui indique le type de signal et le type d'action

 	    //! la fonction fcntl() permet de réaliser des opération sur un file desciptor
            fcntl(serial_port, F_SETOWN, getpid());//!définition du processus auquel est envoyé le SIGIO par son ID
            fcntl(serial_port, F_SETFL, FASYNC);//définition du descripteur de fichier en état asynchrone

}

        // tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
       // tty.c_cflag |= CS8; //! 8 bits per byte (most common)


void UART::interface (struct termios tty)
{

        tty.c_cflag &= ~PARENB; //! Suppression du parity bit
        tty.c_cflag &= ~CSTOPB; //! seulement un stop bit utilisé dans la  communication
        tty.c_cflag &= ~CSIZE; //! désactivation du réglage de la taille des données 
        tty.c_cflag |= CREAD | CLOCAL | CRTSCTS; //!  Activation des paramètre de controle du flux de données



        tty.c_lflag &= ~ICANON; //! désactivation du mode canonique pour pouvoir régler les conditions et temps de lecture dans le serial port
        tty.c_lflag &= ~ECHO; //! Désactivation de l' echo
        tty.c_lflag &= ~ECHOE; //! Désactivation de l' erasure
        tty.c_lflag &= ~ECHONL; //! Désactivation new-line echo
        tty.c_lflag &= ~ISIG; //! Désactivation de l' interpretation de INTR, QUIT and SUSP

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
        tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Désactivation des reglages spéciaux

        tty.c_oflag &= ~OPOST; //! Déxactivation des interprétations spéciales des output bytes (e.g. newline chars)
        tty.c_oflag &= ~ONLCR; //! Désactivation de la  conversion des  newline en carriage return/line feed

        tty.c_cc[VTIME] = 0;    
        tty.c_cc[VMIN] = 1; //! démarrage de la lecture des données dès la réception du premier octet

        //! reglage in/out du baud rate
        cfsetispeed(&tty, B115200);
        cfsetospeed(&tty, B115200);

}

void UART::reception (string& data_string)
{
		unsigned  char buffer[300];
		char oc=';';
        	int nbr=0;
		string str_temp="";

	while(STOP==FALSE && nbr==0)
	{
            memset(&buffer, '\0', sizeof(buffer));

            //printf(".\n");
            usleep(100000);

            /* after receiving SIGIO, wait_flag = FALSE, input is available
            and can be read */

            int lec;
            

            if (wait_flag==FALSE)
            {
            //  int taille=lseek(serial_port,0,SEEK_END);
            // lseek(serial_port,0,SEEK_SET);
                lec = read(serial_port,(void*)buffer,sizeof(buffer));
                tcflush(serial_port,TCOFLUSH);
                printf("nombre d'octets lus: %d\n",lec);
 //               printf("données recues: %s\n",buffer);


                wait_flag = TRUE;      /* wait for new input */
	    }

	nbr = count_if (str_temp.begin(), str_temp.end(), [&oc](char a) { return a == oc; });   

            if(nbr==1)
            {
                //datastr.push_back(';');

//                cout<<"lot de données recues:"<<str_temp<<endl;
		str_temp.pop_back();
		//str_temp.resize(str_temp.size() - 1);
                data_string=str_temp;
                cout<<"string copie: "<<data_string<<endl;
                str_temp="";
//                cout<<"contenu du string apres flush: "<<str_temp<<endl;        
            }

            //data_piece=reinterpret_cast<char*>(buffer);
            //str_temp=str_temp+data_piece;
            str_temp += reinterpret_cast<char*>(buffer);
	}


}


