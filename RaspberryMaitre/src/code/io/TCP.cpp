#include <TCP.hpp>

/**
 * @brief Function who create a TCP/IP connection as a client
 *
 * @return int
 */
int GestionTCP()
{
    // create a socket
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock <= 0)
    {
        cerr << "TCP : can't create a socket !";
        return 0;
    }
    // create hint structure for the server we're connecting with
    int port = 8000;
    string ipAdress = "57.128.34.230";

    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(port);
    inet_pton(AF_INET, ipAdress.c_str(), &hint.sin_addr); // permet de convertir une adresse IP en une adresse IP binaire.

    // connect to the server on the socket
    int connectRes = connect(sock, (sockaddr *)&hint, sizeof(hint));
    if (connectRes == -1)
    {
        cerr << "TCP : can't connect !";
        return 0;
    }

    return sock;
}

/**
 * @brief Function who retrieve data in a JSON file and send it to a server through TCP/IP
 *
 * @param sock
 */
void JSONtoTCP(int sock, bool *is_JSON)
{
    // Open file and collect data
    string toSend;
    string line;
    do
    {
        //      send to the server the json object
        ifstream json_file("data/data.json");
        if (json_file.is_open())
        {
            if (getline(json_file, line))
            {
                *is_JSON = true;
                toSend = line + '\n';
                // Supprime la ligne du fichier json
                system("sed -i '1d' data/data.json");

                json_file.close();
            }
            else
            {
                PLOGI << "Fichier data.json est vide !!" << endl;
                break;
            }
        }
        else
        {
            PLOGI << "Impossible d'ouvrir le fichier data.json" << endl;
            break;
        }

        int sendRes = send(sock, toSend.c_str(), toSend.size() + 1, 0);
        if (sendRes == -1)
        {
            PLOGI << "TCP : Could not send to server!";
        }

        PLOGI << "SERVER> " << endl;

    } while (true);
    // close the socket
    close(sock);
}
