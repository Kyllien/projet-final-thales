#pragma once
/**
 * @file app.hpp
 * @brief Main application
 * Read continously data on the UART port
 * Analyse data to retrieve value of movement detection
 * Activate servo motor at a specified scenario
 * Save sample collected in a JSON file
 * Send JSON regularly JSON file content on TCP to a distant server to centralize data in a database.  
 * @version 1.0
 * @date 2023-01-23
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#include <common.hpp>
#include <pthread.h>
#include <thread>
#include <mutex>
#include <vector>
#include <condition_variable>

#include <TCP.hpp>
#include <PCA9685.hpp>
#include <UART.hpp>


/**
 * @namespace std 
 * 
 */
using namespace std;

/**
 * @namespace nlohmann
 * 
 * @brief Namespace for Json nlohmann library 
 * 
 */
using namespace nlohmann;

/**
 * @class
 * @brief Main application class
 */
class app
{
    public:
        /**
         * @brief Construct a new app object
         * 
         */
        app(){}
        /**
         * @brief Destroy the app object
         * 
         */
        ~app(){this->clean();}
        /**
         * @brief Initialize the app object
         * 
         * @param argc 
         * @param argv 
         * @return true 
         * @return false 
         */
        bool init(int argc, char* argv[]);
        /**
         * @brief Run the app object
         * 
         */
        void run();
        /**
         * @brief Configure the pace of data sampling and the pace of data sending to TCP by reading a JSON file
         * 
         * @return true 
         * @return false 
         */
        bool Configuration(int &data_rate_json, int &data_rate_tcp);
 

    private:
        /**
         * @brief Clean objects created
         * 
         */
        void clean();
        /**
         * @brief Exit the app
         * 
         */
        void exit();

};
        
        /**
         * @brief Fonction accusant reception du signal
         * 
         * @param status 
         */
        void signal_handler_IO (int status);


