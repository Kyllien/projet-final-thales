#pragma once
/**
 * @file common.hpp
 * @brief Common libraries used in majority of files.
 */
#include <iostream>
#include <iomanip>
#include <fstream>
#include <log.hpp>
#include <json.hpp>
#include <chrono>
#include <string.h>

/**
 * @namespace std 
 * 
 */
using namespace std;

/**
 * @namespace nlohmann
 * 
 * @brief Namespace for Json nlohmann library 
 * 
 */
using namespace nlohmann;
