#pragma once

/**
 * @file TCP.hpp
 * @brief TCP client and JSON file transfer from master to server
 * @version 1.0
  * @date 2023-01-23
 */
#include <common.hpp>
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <cstdlib>
#include <fstream>


using namespace std;

/**
 * @brief Function who retrieve data in a JSON file and send it to a server through TCP/IP
 * 
 * @param sock 
 * @param bool
 */
void JSONtoTCP(int, bool*);
/**
 * @brief Function who create a TCP/IP connection as a client
 * 
 * @return int 
 */
int GestionTCP();




