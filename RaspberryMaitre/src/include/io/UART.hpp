/**
 * @file UART.hpp
 * @brief 
 * @version 1.0
 * @date 2023-01-23
 * 
 */




#pragma once
#include <common.hpp>
#include <algorithm>
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()
#include <sys/signal.h>
#include <signal.h>
#include <sys/types.h>
#include <vector>

#define FALSE 0
#define TRUE 1



/**
 * @brief UART 
 * 
 * récpetion des données
 * et formatage en string
 * 
 */
class UART
{
    public:

        /**
         * @brief Construct a new UART object
         * 
         */
        UART();

        /**
         * @brief Destroy the UART object
         * 
         */
        ~UART();

        /**
         * @brief init
         * 
         * méthode qui intialise le programme
         */
        void init();

        /**
         * @brief getSTOP
         * 
         * @return true 
         * @return false 
         */
        bool getSTOP(){return STOP;} 

        /**
         * @brief signal 
         * 
         * méthode qui conditionne 
         * la lecture des données
         * en fonction de la reception
         * du signal
         * 
         * @param saio 
         */
        void signal (struct sigaction saio);
        
        /**
         * @brief reception
         * 
         * méthode qui lit les données
         * à partir du sérial port et
         * les convertit en string
         * 
         * @param str_temp 
         */
        void reception (string& str_temp);
        
        /**
         * @brief interface
         * 
         * méthode qui gère les paramètres
         * de l'interface des ports en série
         * 
         * @param tty 
         */
        void interface (struct termios tty);



    private:
        volatile int STOP; /**<stop*/

        int serial_port; /**<file descriptor*/
        struct termios tty; /**<interface*/
        struct sigaction saio; /**<signal*/

};








