#pragma once
/**
 * @file PCA9685.hpp
 * @brief Driver for control card PCA9685 to control servo-motors with PWM
 * @version 1.0
 * @date 2023-01-23
 * 
 */
#include <cmath>
#include <i2c.hpp>

#define I2C_DEFAULT_ADDRESS 0x40

#define MODE1			0x00
#define MODE2			0x01
#define SUBADR1			0x02
#define SUBADR2			0x03
#define SUBADR3			0x04
#define PRESCALE		0xFE
#define LED0_ON_L		0x06
#define LED0_ON_H		0x07
#define LED0_OFF_L		0x08
#define LED0_OFF_H		0x09
#define ALL_LED_ON_L    0xFA
#define ALL_LED_ON_H	0xFB
#define ALL_LED_OFF_L	0xFC
#define ALL_LED_OFF_H	0xFD
// Bits
#define RESTART			0x80
#define SLEEP			0x10
#define ALLCALL			0x01
#define INVRT			0x10
#define OUTDRV			0x04

#define PAN			    0
#define TILT			1
#define FREQUENCY		50
#define CLOCKFREQ		25000000
#define PANOFFSET		1
#define PANSCALE		1.4
#define TILTOFFSET		30
#define TILTSCALE		1.43
#define PANMAX			270
#define PANMIN			90
#define TILTMAX			90
#define TILTMIN			-45

/**
 * @class PCA9685
 * 
 * @brief Create an PCA9685 controller object with predefined registers adresses
 * and several methods to generate actions with PWM configuration
 * 
 */
class PCA9685 
{
public:
/**
 * @brief Construct a new PCA9685 object
 * 
 * @param i2c_bus 
 * @param i2c_address 
 */
    PCA9685(uint8_t i2c_bus=I2C_DEFAULT_BUS, uint8_t i2c_address=I2C_DEFAULT_ADDRESS );
/**
 * @brief Destroy the PCA9685 object
 * 
 */
    ~PCA9685();

/**
 * @brief Initailize the Pwm object : configure the controller with default parameters.
 * 
 * @return true 
 * @return false 
 */
    bool initialize();
/**
 * @brief Set the Pwm Frequency object : set the PWM frequency.
 * 
 * @param frequency 
 */
    void setPwmFrequency(uint16_t frequency);
/**
 * @brief Set the Pwm object : set pulse width for a defined channel.
 * 
 * @param channel 
 * @param on 
 * @param off 
 */
    void setPwm(uint8_t channel, uint16_t on, uint16_t off);
/**
 * @brief Set the All Pwm object : set pulse width for all channels.
 * 
 * @param on 
 * @param off 
 */
    void setAllPwm(uint16_t on, uint16_t off);
/**
 * @brief Method for moving servo-motors on a channel at a defined angle.
 * 
 * @param channel 
 * @param deg 
 */
    void move(uint8_t channel , int deg);  
 /**
 * @brief Method for initializing all servo-motors at angle 0.
 * 
 */
    bool ServoMoteurs_init();
 /**
 * @brief Scenario of moving yellow flag.
 *  
 * @param angle
 * @param is_movement 
 */   
    void Yellow_servo_scenario(int &angle, bool &is_movement);
/**
 * @brief Scenario of moving blue flag with sample receiption
 * 
 */    
    void Blue_servo_scenario_json();
/**
 * @brief Scenario of moving blue flag with JSON file sending 
 * 
 */    
    void Blue_servo_scenario_tcp();

private:

    i2c *deviceI2C; /**< Create an I2C device object to establish connection and read write on PCA9685 device registers.*/
};


