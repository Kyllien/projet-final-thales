#include "Interface.hpp"

using namespace std;


// Constructeur
Interface::Interface(){}

// Fonction qui permet de savoir si l'utilisateur veut afficher ou pas le contenu de sa recherche
// 1 Affiche / 2 N'affiche pas
int Interface::MenuAffichage(){
    system("clear"); 

    // Differents choix du menu
    vector<string> txt = {"Afficher les donnees","Ne pas afficher les donnees"};

    // Init et affichage du menu, et obtient le choix de l'usager
    Menu menu(txt);
    int choice = menu.ShowAndGetChoice();

    return choice;
}

int Interface::MenuFichier(){
    system("clear"); 

    // Differents choix du menu
    vector<string> txt = {"Enregister dans un fichier","Ne pas enregister dans un fichier"};

    // Init et affichage du menu, et obtient le choix de l'usager
    Menu menu(txt);
    int choice = menu.ShowAndGetChoice();

    return choice;
}

int Interface::RetourMain(){
    // Differents choix du menu
    vector<string> txt = {"Retour au Main Menu","Quitter"};

    // Init et affichage du menu, et obtient le choix de l'usager
    Menu menu(txt);
    int choice = menu.ShowAndGetChoice();

    return choice;
}

// Envois la requete pour récupérer l'ensemble des datas
void Interface::AllData(){

    // Appelle à la fonction pour afficher le menu d'affichage
    int Choix_A = MenuAffichage();

    // Appelle à la fonction pour afficher le menu si il veut enregister un fichier
    int Choix_B = MenuFichier();


    //Envois la requete
    Init_TCP("SELECT b.timestamp, b.balise, b.humidity, b.pressure, b.temperature, h.movement, q.x, q.y, q.z FROM BME b FULL JOIN QMC q ON b.timestamp = q.timestamp FULL JOIN HC h ON b.timestamp = h.timestamp");

    // Affiche ou enregistre les données
    TxtToJson(Choix_A, Choix_B);

    int Choix_C = RetourMain();

    switch(Choix_C){
        case 1:
            MainMenu();
        case 2:
            break;
    }

}

// Fonction qui permet de récuperer les derniere données (n)
void Interface::LastData(int n)
{  
    //Nettoyage de la console
    system("clear");

    // Appelle à la fonction pour afficher le menu d'affichage
    int Choix_A = MenuAffichage();

    // Appelle à la fonction pour afficher le menu si il veut enregister un fichier
    int Choix_B = MenuFichier();

    string req = "SELECT b.timestamp, b.balise, b.humidity, b.pressure, b.temperature, h.movement, q.x, q.y, q.z FROM BME b FULL JOIN QMC q ON b.timestamp = q.timestamp FULL JOIN HC h ON b.timestamp = h.timestamp ORDER BY b.timestamp DESC LIMIT 20";
    Init_TCP(req);

    // Affiche ou enregistre les données
    TxtToJson(Choix_A, Choix_B);

    int Choix_C = RetourMain();

    switch(Choix_C){
        case 1:
            MainMenu();
        case 2:
            break;
    }


}

// Methode qui appelle le menu pour une demande specifique
string Interface::SpecifiqAsk()
{
    //Nettoyage de la console
    system("clear");

    cout << "Quelle detecteur vous intéresse ? (Choix multiples, pour cloturer votre choix veuillez entrer 0)" << endl;
    // Differents choix du menu
    vector<string> txt = {"Capteurs d'humidité","Capteur Triple axis-boussole magnétomètre","Detecteur de mouvement"};

    Menu menu(txt);
    vector<int> rep = menu.ShowAndGetMultipleChoice();

    // Init la requete qui va etre complété au fur et à mesure du processus
    string requeteSelect = "Select timestamp";
    string requeteFrom = " From ";
    string requeteJoin = "";
    string FromJoin = "";
    //string requeteWhere = "Where ";

    // Requete FROM INI
    if(rep[0]==1)
    {
        requeteFrom += " BME b ";
        FromJoin = " ON b.timestamp = ";
        requeteSelect = "Select b.timestamp, b.humidity, b.pressure, b.temperature";
    }
    // Si QMC
    if(rep[0]==2)
    {
        requeteFrom += " QMC q ";
        FromJoin = " ON q.timestamp = ";
        requeteSelect = "Select q.timestamp , q.x, q.y, q.z";
    }
    // Si HC
    if(rep[0]==3)
    {
        requeteFrom += " HC h ";
        FromJoin = " ON h.timestamp = ";
        requeteSelect = "Select h.timestamp, h.movement";
    }

    // JOIN and select 
    if (rep.size() > 1)
    {
        for (int i = 1; i < rep.size(); i++) 
        {
            // Si BME
            if(rep[i]==1)
            {
                requeteJoin += " FULL JOIN BME b " + FromJoin + " b.timestamp ";
                requeteSelect += " , b.humidity, b.pressure, b.temperature";
            }
            // Si QMC
            if(rep[i]==2)
            {
                requeteJoin += " FULL JOIN QMC q " + FromJoin + " q.timestamp ";
                requeteSelect += " , q.x, q.y, q.z";
            }
            // Si HC
            if(rep[i]==3)
            {
                requeteJoin += " FULL JOIN HC h " + FromJoin + " h.timestamp";
                requeteSelect += " , h.movement";
            }
        }
    }

    string req = requeteSelect + requeteFrom + requeteJoin;
    cout << req << endl;
    return req;   
}

void Interface::MenuSpecifiq()
{
    string req = SpecifiqAsk();
    // Appelle à la fonction pour afficher le menu d'affichage
    int Choix_A = MenuAffichage();

    // Appelle à la fonction pour afficher le menu si il veut enregister un fichier
    int Choix_B = MenuFichier();

    Init_TCP(req);
    // Affiche ou enregistre les données
    TxtToJson(Choix_A, Choix_B);
    int Choix_C = RetourMain();

    switch(Choix_C){
        case 1:
            MainMenu();
        case 2:
            break;
    }
}



// Méthode pour initialiser le menu principal
void Interface::MainMenu()
{
    //Nettoyage de la console
    system("clear");   

    // Differents choix du menu
    vector<string> txt = {"Recolte de l'ensemble des données","Afficher les 20 dernières observations","Recolte spécifique","Quitter"};

    // Init et affichage du menu, et obtient le choix de l'usager
    Menu menu(txt);
    int choice = menu.ShowAndGetChoice();

    // En fonction du choix différents chemins sont proposés
    switch(choice){
        case 1:
            AllData();
            break;
        case 2:
            LastData(20);
            break;
        case 3:
            MenuSpecifiq();
            break;
        
        case 4:
            break;
    }
}



