#include "Menu.hpp"

using namespace std;

// Constructeur
Menu::Menu(const vector<string>& choices) : choices_(choices) {}

// Méthode pour afficher le menu et récupérer le choix de l'utilisateur
int Menu::ShowAndGetChoice() 
{
    // Affiche les choix
    //Nettoyage de la console
    for (int i = 0; i < choices_.size(); i++) {
        cout << i + 1 << ". " << choices_[i] << endl;
    }

    int choice;
    cin >> choice; // Récupère le choix de l'utilisateur

    // Vérifie que le choix est valide
    while (choice < 1 || choice > choices_.size()) {
        // Permet d'éviter des problemes au cas si le input ne correspond pas à un string (nettoie le buffer)
        cin.clear(); // réinitialise les indicateurs d'erreur de cin
        cin.ignore(INT_MAX, '\n'); // ignore la ligne entrée

        cout << "Choix non valide, veuillez entrer un chiffre entre 1 et " << choices_.size() << endl;
        cin >> choice;
    }

    return choice;
}

vector<int> Menu::ShowAndGetMultipleChoice()
{
    vector<int> userChoices;
    int choice;
    do {
        for (int i = 0; i < choices_.size(); i++) 
        {
            cout << i + 1 << ". " << choices_[i] << endl;
        }
        cout << "0: Terminer" << endl;
        cin >> choice;
        if (choice > 0 && choice <= choices_.size()) {
            userChoices.push_back(choice);
        }
    } while (choice != 0 && userChoices.size()==0);
    return userChoices;
}


