#include "TCP.hpp"

using namespace std;


void Init_TCP(string req)
{
    int sock = GestionTCP(15000);
    if (sock > 0)
    {
        RequetetoTCP(sock, req);
        TCPtoData(sock);
    }
    
}


int GestionTCP(int port)
{
    // create a socket
    int sock = socket(AF_INET,SOCK_STREAM,0);
    if (sock <= 0)
    {
        cerr << "can't create a socket !";
        return 0;
    }
    // create hint structure for the server we're connecting with 
    string ipAdress = "57.128.34.230";

    sockaddr_in hint ;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(port);
    inet_pton(AF_INET,ipAdress.c_str(), &hint.sin_addr); // permet de convertir une adresse IP en une adresse IP binaire. 

    // connect to the server on the socket
    int connectRes = connect(sock, (sockaddr*)&hint, sizeof(hint));
    if( connectRes == -1)
    {
        cerr << "can't connect !";
        return 0;
    }

    return sock;
}


void RequetetoTCP(int sock, string req)
{
    // Get requete
    //string req = "SELECT * FROM BME";
    int sendRes = send(sock,req.c_str(), req.size()+1, 0);
    if (sendRes == -1)
    {
        cout << "Could not send to server!" << endl;
    }
}


void TCPtoData(int client_socket)
{
    // Réception des réponses du serveur
    char response_data[16384];
    memset(response_data, 0, 16384);
    ssize_t response_len;
    ofstream myfile;
    myfile.open("data/buffer.txt");
    while ((response_len = recv(client_socket, response_data, 16384, 0)) > 0) {
        string response(response_data, response_len);
        //cout << "Received data from server: " << response << endl;
        if(response.substr(response.length() - 7, 7) == "ERROR73")
        {
            cout << "Fin Reception" << endl;
            myfile << response.erase(response.length()- 7, 7) << endl; 
            close(client_socket);
            break;
        }
        myfile << response << endl;
    }
    myfile.close();
}

