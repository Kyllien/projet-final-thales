#include "GestionData.hpp"




using namespace std;

// Fonction qui permet de chercher } dans un string et de retourner sa position
int findChar(string line)
{
    for (int i=0; i < line.length(); i++)
    {
        if(line[i]=='}')
        {
            return i;
        }
    }
    return -1;
}

// Fonction qui permet de créer des objets json à partir de la ligne et retourne
// un string au cas si la ligne qui est terminé est le début d'un nouvel objet
string StringToJson(string line, int ChoixAff, int ChoixFic, ofstream &json_file){
    while(true)
    {
        int pos = findChar(line);
        string str_obj;

        // Si '}' est dans le string alors un objet json est dans le string on laffiche et met dans le fichier 
        // Si '}' n'est pas dans le string il renvoit le string qu'il reste
        if (pos >= 0)
        {
            // Recupere l'objet json
            str_obj= line.substr(0, pos+1);
            nlohmann::json json_obj = nlohmann::json::parse(str_obj);

            if(ChoixAff == 1)
            {
                cout << json_obj.dump() << endl;
            }
            if(ChoixFic == 1)
            {
                json_file << json_obj.dump() + "\n" ;
            }

            line = line.erase(0, pos+1);
        }
        else
        {
            return line;
        }
    }
}

// FOnction qui permet de recupérer les données présentes dans le fichier txt et de le transférer dans le bon format
// En affichant ou en le mettant dans un fichier json
void TxtToJson(int ChoixAff, int ChoixFic){
    string line;
    string surplus = "";
    ofstream json_file;
    ifstream txt_file;
    json_file.open("data/data.json");
    if(json_file.is_open())
    {
        do
        {
            txt_file.open("data/buffer.txt");
            if( txt_file.is_open())
            {
                if(getline(txt_file,line))
                {
                    surplus += line;
                    // Passe de caractere en caractere afin de trouver '}' afin de sauter une ligne dans le fichier json
                    surplus = StringToJson(surplus, ChoixAff, ChoixFic, json_file);
                    // Supprime la ligne du fichier
                    system("sed -i '1d' data/buffer.txt");
                    txt_file.close();
                }
                else
                {   
                    break;
                }
            }      
        }while(true);
        json_file.close();
    }
    else{cout << "Mierda" << endl;} 
}