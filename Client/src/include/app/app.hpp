#pragma once
#include "common.hpp"
#include "log.hpp"


#include "TCP.hpp"
#include "Menu.hpp"
#include "Interface.hpp"

using namespace std;

class app
{
    public:
        app(){}
        ~app(){this->clean();}
        bool init(int argc, char* argv[]);
        void run();
    private:
        void clean();
        void exit();
};