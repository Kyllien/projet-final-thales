#ifndef Menu_HPP
#define Menu_HPP

/**
 * @file Menu.hpp
 * @brief Fichier contenant la classe Menu, qui permet d'afficher des menus dans la console.
 */

#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <climits>

using namespace std;

/**
 * @class Menu
 * @brief 
 * 
 */
class Menu {
    public:
        /**
         * @brief Construct a new Menu object
         * 
         * @param choices 
         */
        Menu(const std::vector<std::string>& choices);

        /**
         * @fn int ShowAndGetChoice()
         * @brief Méthode pour afficher le menu et récupérer le choix de l'utilisateur, pour un menu à un seul choix.
         * 
         * @return int 
         */
        int ShowAndGetChoice();

        /**
         * @fn  vector<int> ShowAndGetMultipleChoice()
         * @brief Méthode pour afficher le menu et récupérer le choix de l'utilisateur, pour un menu avec choix multiples.
         * 
         * @return vector<int> 
         */
        vector<int> ShowAndGetMultipleChoice();

    protected:

    private:
    /**
     * @brief Vecteur de string qui représente les différents choix possibles dans un menu.-
     * 
     */
        std::vector<std::string> choices_;
};


#endif // Menu_HPP