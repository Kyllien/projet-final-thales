#pragma once

/**
 * @file Interface.hpp
 * @brief Gestion des affichages dans la console
 * 
 */
#include "Menu.hpp"
#include "TCP.hpp"
#include "GestionData.hpp"

using namespace std;

/**
 * @class Interface
 * @brief Classe représentant l'interface graphique de la console.
 * 
 */
class Interface {
    public:
        // Constructeur 
        /**
         * @brief Construct a new Interface object.
         * 
         */
        Interface();


        /**
         * @fn void MainMenu()
         * @brief Méthode pour initialiser et afficher le menu principal.
         * 
         */
        void MainMenu();

        /**
         * @fn void AllData()
         * @brief Méthode pour récupérer et afficher toutes les données de la base de données.
         * 
         */
        void AllData();

        /**
         * @fn void LastData(int n)
         * @brief Récupère et affiche les n derniers données insérer dans la base de données.
         * 
         */
        void LastData(int);

        /**
         * @fn string SpecifiqAsk()
         * @brief Fonction qui retourne une requete SQL en focntion des choix de l'utilisateur.
         * 
         * @return string.
         */
        string SpecifiqAsk();

        /**
         * @fn void MenuSpecifiq();
         * @brief Méthode qui permet d'afficher le menu pour une demande spécifique, et d'afficher par la suite son résultat.
         * 
         */
        void MenuSpecifiq();

        /**
         * @brief Fonction qui permet d'afficher un menu afin de savoir si l'utilisateur veut afficher ou pas le contenu de sa recherche.
         * 
         * @return int, qui est le choix de l'utilisateur.
         */
        int MenuAffichage();

        /**
         * @brief Fonction qui permet d'afficher un menu afin de savoir l'utilisateur veut enregistrer ou pas les données.
         * 
         * @return int 
         */
        int MenuFichier();

        /**
         * @brief Fonction qui permet d'afficher un menu afin de savoir si l'utilisateur veut retourné au menu principale.
         * 
         * @return int, qui représente son choix.
         */
        int RetourMain();

};