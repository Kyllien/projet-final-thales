#ifndef TCP_HPP_INCLUDED
#define TCP_HPP_INCLUDED

/**
 * @file TCP.hpp
 * @brief Fichier contenant toutes les fonctions permettant de se connecter au serveur TCP
 */

#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <fstream>
#include <stdlib.h>
//#include <nlohmann/json.hpp>


using namespace std;

/**
 * @fn void Init_TCP(string req)
 * @brief Méthode qui initialise la connexion au serveur TCP à l'aide des autres fonctions.
 * 
 * @param req un string, qui correspond à la requête SQL à envoyer au serveur.
 */
void Init_TCP(string);

/**
 * @fn int GestionTCP(int port)
 * @brief Permet l'initialisation de la socket afin de se connecter au serveur TCP.
 * 
 * @param port un int, qui correspond au port utilisé pour la connexion à la socket.
 * @return int, qui correspond à al socket généré afin de se connecter au serveur.
 */
int GestionTCP(int);

/**
 * @fn void RequetetoTCP(int sock, string req)
 * @brief Méthode qui permet d'envoyer la requête SQL au serveur TCP.
 * 
 * @param sock un int, qui correspond à la socket initialisée au préalable.
 * @param req  un string, qui correspond à la requête SQL à envoyer au serveur TCP.
 */
void RequetetoTCP(int, string);

/**
 * @fn void TCPtoData(int client_socket)
 * @brief Méthode qui récolte les données demandés lors de la requête, ces données sont enregistrées dans un fichier txt.
 * 
 * @param client_socket un int, correspondant à la socket généré en utilisant la fonction GestionTCP.
 */
void TCPtoData(int);


#endif //TCP_HPP_INCLUDED