#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <fstream>
#include "nlohmann/json.hpp"

/*!
* @file GestionData.hpp
* @brief Permet la transformation des données une fois que celles-ci ont été récupérées via la connexion TCP
*/

using namespace std;

/*!
* @fn void TxtToJson(int ChoixAff, int ChoixFic)
* @brief Fonction qui permet de transformer des données contenus dans un fichier .txt en format json afin soit de les afficher soit de les enregistrés dans un .json.
* @param ChoixAff un int qui correspond si la fonction doit afficher ou non les données.
* @param ChoixFic un int qui correspond si la fonction doit enregistrer ou non les données.
*/
void TxtToJson(int, int);


/*!
* @fn int findChar(string line)
* @brief Permet de trouver la position d'un char dans un string et de retourner sa position.
* @param line un string.
* @return un int.
*/
int findChar(string);


/*! 
* @fn string StringToJson(string line, int ChoixAff, int ChoixFic, ofstream &json_file)
* @brief Permet de transformer un string en json et de l'insérer dans un fichier .json et de l'afficher.
* @param line un string à transformer.
* @param ChoixAff un int qui correspond si on doit afficher ou non.
* @param ChoixFic un int qui correspond si on enregistrer un fichier .json ou pas.
* @param json_file une référence à un objet de type ofstream.
* @return un string. Qui correspond au reste de l'objet qui a été découpé lors de l'envoi par TCP.S
*/
string StringToJson(string, int, int, ofstream&);
