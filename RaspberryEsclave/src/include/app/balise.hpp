#pragma once
/**
 * @file balise.hpp
 * 
 * @brief traitement des donn�es
 * @version 0.1
 * @date 2023-01-25
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#include "common.hpp"
#include "log.hpp"
#include "UART.hpp"
#include <fstream>
#include <piqmc5883l.hpp>
#include <capteur.hpp>
#include <HC-SR501.hpp>
#include <bme280.hpp>
#include <time.h>

/**
 * @brief balise 
 * 
 * classe balise
 */
class balise
{
    public:
        /**
         * @brief Construct a new balise object
         * 
         */
        balise(){}
    
        /**
         * @brief Destroy the balise object
         * 
         */
        ~balise(){this->clean();}
        
        /**
         * @brief balise_initialisation
         * 
         * m�thode qui intialise
         * les donn�es capteurs
         * 
         * @return true 
         * @return false 
         */
        bool balise_initialisation();
        
        /**
         * @brief balise_sample_measure 
         * 
         * m�thode qui r�colte les
         *donn�es capteur 
         * 
         * @return json 
         */
        json balise_sample_measure();
        void run();
    private:
         /**
         * @brief clean
         * 
         * m�thode qui clean
         *le programme
         * 
         */
        void clean();
        
        /**
         * @brief exit
         * 
         * m�thode qui sort
         * du programme
         * 
         */
        void exit();
        
        string id = "00001"; /**< idenifiant balise >*/

};
