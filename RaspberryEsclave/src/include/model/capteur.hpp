#pragma once

#include "json.hpp"
#include <string>

using namespace nlohmann;
using namespace std;

class capteur
{
    public:
        capteur(string id){this->id=id;}
        virtual ~capteur(){}

        string getId(){return id;}
        void setId(string id){this->id=id;}

        virtual bool initialize()=0;

        virtual bool measure()=0;
        
        json getSample(){return sample;} 
    
    protected:
        json sample;

    private:
        string id;
        
};

