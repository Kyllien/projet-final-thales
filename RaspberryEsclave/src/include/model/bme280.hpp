#ifndef BME280_H_INCLUDED
#define BME280_H_INCLUDED

/**
 * @file bme280.hpp
 * @brief Driver of BME280
 * Sensor who measure humidity, temperature and pressure of the environment
 * @version 1.0
 * @date 2023-01-23
 * 
 */
#include <iostream>
#include <iomanip>
#include "i2c.hpp"
#include <stdint.h>       // pour les types uint8_t etc
#include <math.h>
#include <capteur.hpp>


#define I2C_DEFAULT_ADDRESS   0x76
//Register names:

#define DIG_T1        0x88
#define DIG_T2        0x8A
#define DIG_T3        0x8C
#define DIG_P1        0x8E
#define DIG_P2        0x90
#define DIG_P3        0x92
#define DIG_P4        0x94
#define DIG_P5        0x96
#define DIG_P6        0x98
#define DIG_P7        0x9A
#define DIG_P8        0x9C
#define DIG_P9        0x9E
#define DIG_H1        0xA1
#define DIG_H2        0xE1
#define DIG_H3        0xE3
#define DIG_H4        0xE4
#define DIG_H5        0xE5
#define DIG_H6        0xE7
#define CHIPID        0xD0
#define VERSION       0xD1
#define SOFTRESET     0xE0
#define BME280_RESET  0xB6
#define CAL26         0xE1
#define CONTROLHUMID  0xF2
#define STATUS        0xF3
#define CONTROL       0xF4
#define CONFIG        0xF5
#define PRESSUREDATA  0xF7
#define TEMPDATA      0xFA
#define HUMIDDATA     0xFD

#define MEAN_SEA_LEVEL_PRESSURE         1013


//  les registres des données pour la pression température et humidité
/**
 * @struct bme280_raw_data
 * @brief Structure of data register for retrieving pressure, temperature and humidity
 * 
 */
typedef struct
{
  uint8_t pmsb;
  uint8_t plsb;
  uint8_t pxsb;

  uint8_t tmsb;
  uint8_t tlsb;
  uint8_t txsb;

  uint8_t hmsb;
  uint8_t hlsb;

  uint32_t temperature;
  uint32_t pressure;
  uint32_t humidity;

} bme280_raw_data;


// structure pour enregistrer les constantes  de calibration
/**
 * @struct bme280_calib_data
 * @brief Structure for calibarating raw datas to usable datas
 * 
 */
typedef struct
{
  uint16_t dig_T1;
  int16_t  dig_T2;
  int16_t  dig_T3;

  uint16_t dig_P1;
 int16_t  dig_P2;
  int16_t  dig_P3;
  int16_t  dig_P4;
  int16_t  dig_P5;
  int16_t  dig_P6;
  int16_t  dig_P7;
  int16_t  dig_P8;
  int16_t  dig_P9;

  uint8_t  dig_H1;
  int16_t  dig_H2;
  uint8_t  dig_H3;
  int16_t  dig_H4;
  int16_t  dig_H5;
  int8_t   dig_H6;

  int32_t  t_fine;
} bme280_calib_data;

/**
 * @class bme280
 * @brief Class of bme280 sensor with preconfigured registers adresses
 * 
 */
class bme280 : public capteur
{

public:
    /**
     * @brief Construct a new bme280 object
     * 
     * @param i2cBus 
     * @param i2cAddress 
     */
    bme280(int i2cBus=I2C_DEFAULT_BUS, int i2cAddress=I2C_DEFAULT_ADDRESS);
    /**
     * @brief Destroy the bme280 object
     * 
     */
    ~bme280();
    /**
     * @brief Verify existence of the device on I2C bus
     * 
     * @return true 
     * @return false 
     */
    bool obtenirErreur();

    // Méthode pour obtenir le Chip ID (0x60 pour le BME280)
    /**
     * @brief Retrieve the chip ID (register 0x60 for BME280)
     * 
     * @return unsigned int 
     */
    unsigned int obtenirChipID();

    // méthodes pour lire la température le pression et l'humidité
    /**
     * @brief Measure temperature in celsius
     * 
     * @return double 
     */
    double obtenirTemperatureEnC();
    /**
     * @brief Measure temperature in fahrenheit
     * 
     * @return double 
     */
    double obtenirTemperatureEnF();
    /**
     * @brief Measure absolute pressure
     * 
     * @return double 
     */
    double obtenirPression();
    /**
     * @brief Measure humidity
     * 
     * @return double 
     */
    double obtenirHumidite();

    // methode pour obtenir la pression au niveau de la mer
    /**
     * @brief Set altitude for calibration
     * 
     * @param h 
     */
    void donnerAltitude(double h);
    /**
     * @brief Calculate pressure at sea level
     * 
     * @return double 
     */
    double obtenirPression0();

    // méthode pour obtenir la valeur du point de rosée
    /**
     * @brief Calculate dew point 
     * 
     * @return double 
     */
    double obtenirPointDeRosee();

    // methode pour obtenir la version
    /**
     * @brief Get version of BME 280 sensor driver
     * 
     */
    void version();
    /**
     * @brief Initialize BME 280 sensor to preconfigured calbration value and innstanciation of an I2C connection
     * 
     * @return true 
     * @return false 
     */
    bool initialize() override;
    /**
     * @brief Overall measure of pressure, temperature and humidity
     * 
     * @return true 
     * @return false 
     */
    bool measure() override;


private:


    i2c *deviceI2C;                   /**< i2c file descriptor*/
    bme280_calib_data cal;           
    bme280_raw_data raw;             
    double h;                         /**<  altitude of Sensor reative to sea level*/
    bool error;                       /**< store error value*/
    /**
     * @brief Get calbration preconfigured data
     * 
     */
    void readCalibrationData();
    /**
     * @brief Get the Raw Data for registers
     * 
     */
    void getRawData();
};

#endif // BME280_H_INCLUDED
