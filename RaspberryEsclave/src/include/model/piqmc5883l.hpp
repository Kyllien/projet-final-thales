#ifndef PIQMC5883L_H
#define PIQMC5883L_H
/**
 * @file piqmc5883l.hpp
 * @brief Driver of QMC5883L 
 * Sensor who measure direction of earth magnetic field on 3-axis
 * @version 1.0
 * @date 2023-01-23
 * 
 */
#include "capteur.hpp"
#include <vector>
#include <cstdint>
#include <cmath>
#include <i2c.hpp>

using namespace std;

#define I2C_DEFAULT_ADRESSE      0x0D

/* Register numbers */
#define QMC5883L_X_LSB          0x00
#define QMC5883L_X_MSB          0x01
#define QMC5883L_Y_LSB          0x02
#define QMC5883L_Y_MSB          0x03
#define QMC5883L_Z_LSB          0x04
#define QMC5883L_Z_MSB          0x05
#define QMC5883L_STATUS         0x06
#define QMC5883L_TEMP_LSB       0x07
#define QMC5883L_TEMP_MSB       0x08
#define QMC5883L_CONFIG         0x09
#define QMC5883L_CONFIG2        0x0A
#define QMC5883L_RESET          0x0B
#define QMC5883L_RESERVED       0x0C
#define QMC5883L_CHIP_ID        0x0D

/* Values for the STATUS register */
#define QMC5883L_STATUS_DRDY    0x01        //Data ready
#define QMC5883L_STATUS_OVL     0x02        //Overflow flag
#define QMC5883L_STATUS_DOR     0x04        //Data skipped for reading

/* Values for the CONFIG2 register */
#define QMC5883L_CONFIG2_IENB   0x01        //Interrupt Pin Enabling.
#define QMC5883L_CONFIG2_POL    0x40        //Pointer Roll-over.
#define QMC5883L_CONFIG2_SRST   0x80        //Soft Reset.

/* Oversampling values for the CONFIG register */
#define QMC5883L_CONFIG_OS512   0x00
#define QMC5883L_CONFIG_OS256   0x40
#define QMC5883L_CONFIG_OS128   0x80
#define QMC5883L_CONFIG_OS64    0xC0

/* Range values for the CONFIG register */
#define QMC5883L_CONFIG_2GAUSS  0x00
#define QMC5883L_CONFIG_8GAUSS  0x10

/* Rate values for the CONFIG register */
#define QMC5883L_CONFIG_10HZ    0x00
#define QMC5883L_CONFIG_50HZ    0x04
#define QMC5883L_CONFIG_100HZ   0x08
#define QMC5883L_CONFIG_200HZ   0x0C

/* Mode values for the CONFIG register */
#define QMC5883L_CONFIG_STANDBY 0x00
#define QMC5883L_CONFIG_CONT    0x01

#define DECLINATION 3.339
/**
 * @class PiQMC588L
 * @brief Class of Sensor QMC5883L with preconfigured registers adresses
 * 
 */
class PiQMC5883L : public capteur
{
  
    public:
        /**
         * @brief Construct a new QMC5883L object
         * 
         * @param i2c_bus 
         * @param i2c_address 
         */
        PiQMC5883L(const uint8_t i2c_bus=I2C_DEFAULT_BUS, const uint8_t i2c_address=I2C_DEFAULT_ADRESSE);
        /**
         * @brief Destroy the QMC5883L object
         * 
         */
        ~PiQMC5883L();
        /**
         * @brief Initialize the QMC5883L with preconfigure values
         * 
         * @return true 
         * @return false 
         */
        bool initialize() override;
        /**
         * @brief Ask if new data are readable
         * 
         * @return true 
         * @return false 
         */
        bool isDataReady();
        /**
         * @brief First method to measure raw data and calculate a magnetic field declination adjusted direction.
         * 
         * @return int 
         */
        int readCalibratedHeading();
        /**
         * @brief First method to measure raw data and calculate a magnetic field declination adjusted direction.
         * 
         * @return int 
         */
        int readCalibratedHeading2();
        /**
         * @brief First method to measure raw data and calculate a magnetic field declination adjusted direction.
         * 
         * @return int 
         */
        int readCalibratedHeading3();

        /**
         * @brief Method to soft reset QMC5883L
         * 
         */
        void softReset();
        /**
         * @brief Define the reset period to set in reset register
         * 
         */
        void defineSetResetPeriod();
        /**
         * @brief Disable interrupt mode
         * 
         */
        void disableInterrupt();
        /**
         * @brief Set the predefined configuration
         * 
         */
        void setConfiguration();
        /**
         * @brief Set the mode read / write
         * 
         * @param x 
         */
        void setMode(const int &x);
        /**
         * @brief Set the Oversampling
         * 
         * @param x 
         */
        void setOversampling(const int &x);
        /**
         * @brief Set the range of magnetic sensitivity
         * 
         * @param x 
         */
        void setRange(const int &x);
        /**
         * @brief Set the Sampling Rate
         * 
         * @param x 
         */
        void setSamplingRate(const int &x);
        /**
         * @brief Measure raw data
         * 
         * @param x 
         * @param y 
         * @param z 
         * @return true 
         * @return false 
         */
        bool readRaw(int16_t *x, int16_t *y, int16_t *z);
        /**
         * @brief Calibrate the sensor with calibration offsets
         * 
         */
        void calibrate();
        /**
         * @brief Set the Calibration Offsets
         * 
         * @param c1 
         * @param c2 
         * @param c3 
         * @param c4 
         * @param c5 
         * @param c6 
         */
        void setCalibrationOffsets(const int &c1, const int &c2, const int &c3,
                            const int &c4, const int &c5, const int &c6);
        /**
         * @brief Set the Calibration Offsets 
         * 
         * @param c1 
         * @param c2 
         * @param c3 
         * @param c4 
         * @param c5 
         * @param c6 
         * @param c7 
         * @param c8 
         * @param c9 
         */
        void setCalibrationOffsets(const float &c1, const float &c2, const float &c3,
                            const float  &c4, const float &c5, const float &c6,
                            const float &c7, const float &c8, const float &c9);
        /**
         * @brief Reset the Calibration offsets
         * 
         */
        void resetCalibrationOffsets();
        /**
         * @brief Set the Declination
         * 
         * @param declination 
         */
        void setDeclination(const float &declination);
        /**
         * @brief Reset the Declination
         * 
         */
        void resetDeclination();
        /**
         * @brief Retrieve the raw measure
         * 
         * @return true 
         * @return false 
         */
        bool measure() override;

    private:
        i2c *deviceI2C; /**< i2c object*/

    //   const uint8_t i2c_bus;
    //   const uint8_t i2c_address;

        vector<vector<float>> calibration2; /**<calibration offsets*/
        vector<vector<int>> calibration; /**<calibration offsets*/
        float declination = 10.0;   /**< declination */

        uint8_t mode; /**< mode */
        uint8_t rate;   /**< sampling rate*/
        uint8_t range;  /**< range of magnetic sensitivity*/
        uint8_t oversampling; /**< oversampling */

        int xlow,ylow,xhigh,yhigh=0; /**MSB LSB values*/
};

#endif // PIQMC5883L_H
