#pragma once

#include "capteur.hpp"
#include <wiringPi.h>
#include <stdio.h>
#include <string>


class HCSR501: public capteur
{
    public:
        HCSR501(int PIR_PIN);
        ~HCSR501(){}

        int getPIR_PIN(){return PIR_PIN;}
        void setPIR_PIN(int val){PIR_PIN=val;}

        bool initialize() override;

        bool measure() override;

    private:
        int PIR_PIN;

};
