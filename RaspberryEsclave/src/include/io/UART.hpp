#pragma once
#include "log.hpp"

#include <fstream>

#include <time.h>
#include <common.hpp>
#include <algorithm>
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()
#include <sys/signal.h>
#include <signal.h>
#include <sys/types.h>
#include <vector>

#define FALSE 0
#define TRUE 1



/**
 * @brief UART 
 * 
 * récpetion des données
 * et formatage en string
 * 
 */
class UART
{
    public:

        /**
         * @brief Construct a new UART object
         * 
         */
        UART();

        /**
         * @brief Destroy the UART object
         * 
         */
        ~UART();

        /**
         * @brief init
         * 
         * méthode qui intialise le programme
         */
        void init();

      
      
        
        /**
         * @brief transmettre
         * 
         * méthode qui ecrit les données
         * dans le sérial port et
         * les convertit en string
         * 
         * @param datastr 
         */
        void transmettre (string datastr);
        
        /**
         * @brief interface
         * 
         * méthode qui gère les paramètres
         * de l'interface des ports en série
         * 
         * @param tty 
         */
        void interface (struct termios tty);
        

    private:

        int serial_port; /**<file descriptor*/
        struct termios ttys; /**<interface*/

};
