#pragma once
/**
 * @file i2c.hpp
 * @brief Api for communication with I2C protocol using ioctl and libi2c-dev SMBus functions
 * @version 1.0
 * @date 2023-01-23
 * 
 * 
 */
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/ioctl.h>
//#include <asm/ioctl.h>
#include <iostream>
#include <log.hpp>


#define I2C_DEFAULT_BUS 1

// I2C definitions

#define I2C_SLAVE	0x0703  /* Use this slave address */
#define I2C_ACK_TEST	0x0710	/* Voir si un esclave est à une adresse spécifique */
#define I2C_SMBUS	0x0720	/* SMBus transfer */


#define I2C_SMBUS_READ	1
#define I2C_SMBUS_WRITE	0

// SMBus transaction types

#define I2C_SMBUS_QUICK		    0  // This sends a single bit to the device, at the place of the Rd/Wr bit.
#define I2C_SMBUS_BYTE		    1  // Handles the SMBus read_byte and write_byte commands
#define I2C_SMBUS_BYTE_DATA	    2  // Handles the SMBus read_byte_data and write_byte_data commands
#define I2C_SMBUS_WORD_DATA	    3  // Handles the SMBus read_word_data and write_word_data commands
#define I2C_SMBUS_PROC_CALL	    4  // This command selects a device register (through the Comm byte), sends
                                       // 16 bits of data to it, and reads 16 bits of data in return.
#define I2C_SMBUS_BLOCK_DATA	    5  // Handles the SMBus read_block_data and write_block_data commands
#define I2C_SMBUS_I2C_BLOCK_BROKEN  6  // 
#define I2C_SMBUS_BLOCK_PROC_CALL   7  // This command selects a device register (through the Comm byte), sends
				       // 1 to 31 bytes of data to it, and reads 1 to 31 bytes of data in return.
#define I2C_SMBUS_I2C_BLOCK_DATA    8

// SMBus messages

#define I2C_SMBUS_BLOCK_MAX	32	/* taille maxi d'un bloc de données */
#define I2C_SMBUS_I2C_BLOCK_MAX	32	/* Not specified but we use same structure */

// Structures utilisées par les appels ioctl()

using namespace std;

// La donnée peut être soit un Octet, soit un Mot ou un tableau d'octet
/**
 * @union i2c_smbus_data
 * @brief Union of different size of data to read or write 
 */
union i2c_smbus_data
{
    uint8_t  byte ;
    uint16_t word ;
    uint8_t  block [I2C_SMBUS_BLOCK_MAX + 2] ;	// 2 car block [0] est utilisé pour la longeur + 1 pour PEC (Controle CRC)
};
/**
 * @struct i2c_smbus_ioctl_data
 * @brief Structure of ioctl parameters
 * 
 */
struct i2c_smbus_ioctl_data
{
    char read_write ;
    uint8_t command ;
    int size ;
    union i2c_smbus_data *data ;
};


/**
 * @class i2c
 * @brief Class for instanciating i2c device connection
 * 
 */
class i2c
{

    public:

        /**
         * @brief Construct a new i2c object
         * 
         * @param i2c_bus 
         * @param i2c_address 
         */
        i2c(int i2c_bus, int i2c_address);

        //idBusI2C = 0 pour les raspberry version 1
        //idBusI2C = 1 pour les raspberry version 2, 3 et 4

        /**
         * @brief Destroy the i2c object
         * 
         */
        ~i2c();
        
        /**
         * @brief Initialize an i2 connection
         * 
         */
        void i2cInit();
        /**
         * @brief Get the stored error code of the object
         * 
         * @return int 
         */
        int getError();
        /**
         * @brief Read registers bit by bit
         * 
         * @return unsigned char 
         */
        unsigned char Read ();
        /**
         * @brief Read LSB in registers bytes by bytes 
         * 
         * @param reg 
         * @return unsigned char 
         */
        unsigned char ReadReg8 (int reg);
        /**
         * @brief Read MSB in registers bytes by bytes
         * 
         * @param reg 
         * @return unsigned short 
         */
        unsigned short ReadReg16 (int reg);
        /**
         * @brief Read block of bytes in registers
         * 
         * @param reg 
         * @param length 
         * @param values 
         * @return int 
         */
        int ReadBlockData (int reg, int length, int *values);

        /**
         * @brief Write bit by bit in registers
         * 
         * @param data 
         */
        void Write (int data);
        /**
         * @brief Write LSB in register bytes by bytes
         * 
         * @param reg 
         * @param value 
         */
        void WriteReg8 (int reg, int value);
        /**
         * @brief Vrite MSB in registers bytes by bytes
         * 
         * @param reg 
         * @param value 
         */
        void WriteReg16 (int reg, int value);
        /**
         * @brief Write block of data in registers
         * 
         * @param reg 
         * @param length 
         * @param data 
         */
        void WriteBlockData (int reg, int length, int *data);

      private:
        int i2c_bus; /**< i2c bus port*/
        int i2c_address; /**< i2c device adress*/
        int fd; /**< i2c device file */
        int error; /**< error number */
        /**
         * @brief i2c_smbus_access function to call registers.
         * 
         * @param rw 
         * @param command 
         * @param size 
         * @param data 
         */
        inline void i2c_smbus_access (char rw, uint8_t command, int size, union i2c_smbus_data *data);

};

