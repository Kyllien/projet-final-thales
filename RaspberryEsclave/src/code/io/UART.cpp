#include <UART.hpp>



UART::~UART()
{
        close(serial_port);
}

UART::UART()
{
        init();
        PLOGI << "UART ready";
}

void UART::init()
{
    
//        this->serial_port = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NONBLOCK);
        int serial_port=open("/dev/serial0",O_RDWR);
        
        if(tcgetattr(serial_port, &ttys) != 0) {
        
            PLOGE << "Serial tcgetattr error : " << strerror(errno);
        }

        interface(ttys);

        // tcflush(serial_port, TCIFLUSH);
        
        // Save tty settings, also checking for error
        if (tcsetattr(serial_port, TCSANOW, &ttys) != 0) 
        {
             PLOGE << "Serial tcsetattr error : " << strerror(errno);
        }
        else
        {
             PLOGI << "parametres de l'interface validés";
        }
}



     


void UART::interface (struct termios tty)
{

  tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
  tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
  tty.c_cflag &= ~CSIZE; // Clear all bits that set the data size 
  tty.c_cflag |= CS8; // 8 bits per byte (most common)
  tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
  tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)

  tty.c_lflag &= ~ICANON;
  tty.c_lflag &= ~ECHO; // Disable echo
  tty.c_lflag &= ~ECHOE; // Disable erasure
  tty.c_lflag &= ~ECHONL; // Disable new-line echo
  tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
  tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
  tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of >

  tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
  tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed



// Set in/out baud rate to be 9600
  cfsetispeed(&tty, B115200);
  cfsetospeed(&tty, B115200);
}

void UART::transmettre (string datastr)
{
      
      cout<<"string: "<<datastr<<endl;
      int size=datastr.length()+1;
      char* c=new char[size];
      printf("taille du string: %d\n",size);
    	strcpy(c,datastr.c_str());
       
        int trans= write(serial_port,c,size);
    
    
        printf("nombre d'octets transmis via le port série: %d\n",trans);
        usleep(500000);
        

}



