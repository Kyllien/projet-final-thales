
#include <balise.hpp>
#include <piqmc5883l.hpp>
#include <capteur.hpp>
#include <HC-SR501.hpp>
#include <bme280.hpp>
#include <time.h>


using namespace std;
 HCSR501 Detecteur(0);
 PiQMC5883L Compas;
 bme280 Meteo;


bool balise::balise_initialisation()
{
    PLOGI << "Lancement de l'initialisation";
    try
    {
        PLOGI << "Sensors created" ;

        if(!Detecteur.initialize()) {
            PLOGE << "Sensor " << Detecteur.getId() << " HS at init";
        }
        if(!Compas.initialize()) {
            PLOGE << "Sensor " << Compas.getId() << " HS at init";
        }
        if(!Meteo.initialize()) {
            PLOGE << "Sensor " << Meteo.getId() << " HS at init";
        }
        PLOGI << "Sensors Initialized" ;

        return true;
    }

    catch(const exception& e)
    {
        PLOGF << "Erreur d'exécution le programme va fermer: " << e.what();

         return false;
    }

}



json balise::balise_sample_measure()
{

    PLOGI << "Lancement des mesures";
    try
    {
        json data;
        while(true)
        {
                data["Balise"] = id;
                data["Timestamp"]= time(0);

                if(!Detecteur.measure()) {
                    data[Detecteur.getId()]= Detecteur.getSample();
                    data[Detecteur.getId()]["State"] = "HS";
                    PLOGI <<  Detecteur.getId() << " : measure error";
                }
                else {
                    data[Detecteur.getId()]= Detecteur.getSample();
                    data[Detecteur.getId()]["State"] = "OK";

                }

                if(!Compas.measure()) {
                    data[Compas.getId()]= Compas.getSample();
                    data[Compas.getId()]["State"] = "HS";
                    PLOGI <<  Compas.getId() << " : measure error";
                }
                else {
                    data[Compas.getId()]= Compas.getSample();
                    data[Compas.getId()]["State"] = "OK";
                }

                if(!Meteo.measure()) {
                    data[Meteo.getId()]= Meteo.getSample();
                    data[Meteo.getId()]["State"] = "HS";
                    PLOGI <<  Meteo.getId() << " : measure error";
                }
                else {
                    data[Meteo.getId()]= Meteo.getSample();
                    data[Meteo.getId()]["State"] = "OK";
                }

            return data;
        }

    }

    catch(const exception& e)
    {
        PLOGF << "Erreur d'exécution le programme va fermer: " << e.what();
    }

}

void balise::run()
{
    PLOGI << "Lancement du programme";
    try
    {     
    
      UART UART;
      string datastr;
      while(true) { 
          datastr = balise_sample_measure().dump()+";";
 //         UART.transmettre(datastr); 
           cout<<"string: "<<datastr<<endl;
      int size=datastr.length()+1;
      char* c=new char[size];
      printf("taille du string: %d\n",size);
    	strcpy(c,datastr.c_str());
       
        int trans= write(open("/dev/serial0",O_RDWR),c,size);
    
    
        printf("nombre d'octets transmis via le port série: %d\n",trans);
        usleep(500000);
      }
    
	 
    }

    catch(const exception& e)
    {
        PLOGF << "Erreur d'exécution le programme va fermer: " << e.what();
    }

    this->exit();
}


void balise::clean()
{
    PLOGD << "Clean du programme";
}

void balise::exit()
{
    PLOGW << "Sortie  du programme";
}

