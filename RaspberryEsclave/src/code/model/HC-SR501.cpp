#include <HC-SR501.hpp>
#include <log.hpp>

HCSR501::HCSR501(int pin)
    :capteur("HC-SR501")
{
    this->PIR_PIN = pin;
}

bool HCSR501::initialize()
{
        if (wiringPiSetup() == -1){
                PLOGE << getId() << " Initialization: FAILURE";
                return false;
        }

        pinMode(getPIR_PIN(),INPUT);
        PLOGI << getId() << " Initialization: SUCCESS";
        PLOGI << errno;
        return true;
}

bool HCSR501::measure()
{
        if(digitalRead(getPIR_PIN()) == 1)
        {
                sample["Movement"] = 1;
        }
        else
        {
                sample["Movement"] = 0;
        }
        return true;
}
