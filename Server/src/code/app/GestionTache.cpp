/**
 * @file GestionTache.cpp
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#include "GestionTache.hpp"

// PASSE PAR 2 fihciers avec un 3 eme thread qui lui permet de faire un copier colle en utilisant les commandes system
// avec un sleep de 2 min qui permet à ce que celui-ci

/**
 * @brief Fonction permettant de gérer deux fils d'exécutions utilisant une base de données.
 * 
 */
void InitTache()
{
    // Global Variables
    // besoin d'un seul mutex pour bloquer l'accès à la base de donnée car risque de conflit
    mutex mtx;

    thread thread_Raspberry([&mtx] () {
        while(true)
        {
            int serverSocket = Init_SocketServer(8000);
            int clientSocket = Init_SocketClient(serverSocket);
            if(clientSocket > 0)
            {
                std::unique_lock<std::mutex> lock(mtx);        
                Reception_Data(clientSocket);
                lock.unlock();
            }                    
            close(serverSocket); 
            cout << "FIN" << endl;
            sleep(1); //OBLIGATOIRE POUR QUE CA FONCTIONNE, DONT KNOW WHY BUT THIS IS GOOD
        }
           
    });

    
    thread thread_Client([&mtx] () {
        while(true)
        {
            // Dans un premier temps il attend une requete, une fois recu il l'a transmet
            // Une fois recu via Gestion_Recept.. il rentre dnas le mutex qui lui donnne accès
            // A la base de donnée, une fois la requete SQL effectué il récupère les datas 
            // Et les insère dans un fichier json afin d'être récupéré et envoyé par la suite
            // Ensuite on delock la base de donnée puis on recupere le fichier json et envoit ces données
            // En mettant en place une nouvelle connexion
            int serverSocket = Init_SocketServer(15000);
            int clientSocket = Init_SocketClient(serverSocket);
            string requete = Reception_Requete(clientSocket);
            // Tant que la port ne recoit pas de requete valide il n'a pas besoin d'avoir accès à la base de donnée
            if (requete != "0")
            {
                std::unique_lock<std::mutex> lock(mtx);
                // Recolte les datas à partir de la requête SQL 
                Get_Table2(requete.c_str(), clientSocket);
                lock.unlock();
                close(clientSocket);
            }
            sleep(1);
            close(serverSocket);

        }
    });


    thread_Raspberry.join();
    thread_Client.join();
}

