/**
 * @file app.cpp
 * @brief classe permettant d'initialiser et d'exécuter le programme du serveur
 * @copyright Copyright (c) 2023
 * 
 */

#include "app.hpp"

/**
 * @brief Fonction qui initialise le programme
 * 
 */

/**
 * @brief 
 * 
 * @param argc 
 * @param argv 
 * @return true 
 * @return false 
 */
bool app::init(int argc, char* argv[])
{
    try
    {
        init_logs(argc >1 ? string(argv[1]): string(""));
        PLOGI << "Initialisation du programme";
        return true;
    }
    catch(const std::exception& e)
    {
        PLOGF << "Erreur à l'initialisation:" << e.what();
        return false;
    }
}


/**
 * @brief Fonction qui exécute le programme serveur
 * 
 */
void app::run()
{
    PLOGI << "Lancement du programme";
    try
    {
        InitTache();

    }
    catch(const exception& e)
    {
        PLOGF << "Erreur d'exécution le programme va fermer: " << e.what();
    }
    
    this->exit();
}

/**
 * @brief Clean le programme
 * 
 */
void app::clean()
{
    PLOGD << "Clean du programme";
}

/**
 * @brief Sortie du programme
 * 
 */
void app::exit()
{
    PLOGW << "Sortie  du programme";
}