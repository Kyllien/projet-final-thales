#include "csvSerializer.hpp"

/**
 * @brief Ouvre un fichier CSV pour la lecture
 * @param path Le chemin d'accès au fichier CSV
 * @param skipHeader Indique s'il faut ignorer la première ligne (header)
 * @param separator Le caractère séparateur utilisé dans le fichier CSV
 * @return true si le fichier a été ouvert avec succès, false sinon
 */
bool CSVSerializer::openReader(string path, bool skipHeader, char separator)
{
    if(this->file.is_open())
    {
        this->file.close();
    }
    this->file.open(path, ios_base::in);
    bool result = this->file.is_open();
    if(result && skipHeader)
    {
        string line;
        getline(this->file, line);
    }
    this->separator = separator;
    this->is_read = true;
    return result;
}



/**
 * @brief Ouvre un fichier CSV pour l'écriture
 * @param path Le chemin d'accès au fichier CSV
 * @param append Indique s'il faut ajouter les données à la fin du fichier existant ou écraser le fichier
 * @param separator Le caractère séparateur à utiliser pour les données écrites
 * @return true si le fichier a été ouvert avec succès, false sinon
 */
bool CSVSerializer::openWriter(string path, bool append, char separator)
{
    if(this->file.is_open())
    {
        this->file.close();
    }
    this->file.open(path, append? (ios_base::out | ios_base::app): ios_base::out);
    bool result = this->file.is_open();
    this->separator = separator;
    this->is_read = false;
    return result;
}

/**
 * @brief Vérifie si l'objet est prêt à écrire dans le fichier CSV
 * @return true si l'écriture est possible, false sinon
 */
bool CSVSerializer::canWrite()
{
    if(this->is_read || !this->file.good())
    {
        PLOGD << "Écriture impossible due à l'état du fichier";
        return false;
    }
    return true;
}

/**
 * @brief Vérifie si l'objet est prêt à lire le fichier CSV
 * @return true si la lecture est possible, false sinon
 */
bool CSVSerializer::canRead()
{
    if(!this->is_read || !this->file.good())
    {
        PLOGD << "Lecture impossible due à l'état du fichier";
        return false;
    }
    return true;
}

/**
 * @brief Écrit les propriétés de l'objet sérialisable en tant qu'en-tête (header) dans le fichier CSV
 * @param s L'objet sérialisable dont les propriétés doivent être écrites
 * @return true si l'en-tête a été écrit avec succès, false sinon
 */
bool CSVSerializer::writeHeader(const Serializable& s)
{
    if(this->canWrite())
    {
        vector<string> properties= s.getProperties();
        int i=0;
        int last = properties.size()-1;
        for(string p: properties)
        {
            this->file << p;
            if(i++<last)
            {
                this->file << this->separator;
            }
        }
        this->file << endl;
        return true;
    }
    return false;
}

/**
 * @brief Écrit les données de l'objet sérialisable dans le fichier CSV
 * @param s L'objet sérialisable dont les données doivent être écrites
 * @return true si les données ont été écrites avec succès, false sinon
 */
bool CSVSerializer::write(const Serializable& s)
{
     if(this->canWrite())
    {
        ExchangeItem e = s.to_exchangeItem();
        vector<string> values = e.getValues();
        int i=0;
        int last = values.size()-1;
        for(string p: values)
        {
            this->file << p;
            if(i++<last)
            {
                this->file << this->separator;
            }
        }
        this->file << endl;
        return true;
    }
    return false;
}
/**
 * @brief Lit les données du fichier CSV et les assigne à l'objet sérialisable
 * @param s L'objet sérialisable qui doit recevoir les données lues
 * @return true si des données ont été lues et assignées à l'objet, false sinon
 */
bool CSVSerializer::read(Serializable& s)
{
    bool result = false;
    if(canRead())
    {
        vector<string> row;
        string line;
        string word;
        vector<string> properties = s.getProperties();
        ExchangeItem e = s.to_exchangeItem();
        while(getline(this->file, line))
        {
            row.clear();
            stringstream str(line);
            while(getline(str, word, this->separator))
            {
                row.push_back(word);
            }
            if(row.size()!=properties.size())
            {
                continue;;
            }
            handle_carriage_return(row);
            int i=0;
            for(string p: properties)
            {
                e[p] = row[i++];
            }
            s.from_exchangeItem(e);
            result = true;
        }
        if(!result)
        {
            this->file.close();
        }
    }
    return result;
}

/**
 * @brief Ferme le fichier CSV ouvert
 * @return true si le fichier a été fermé avec succès, false sinon
 */
bool CSVSerializer::close()
{
    this->file.close();
    return !this->file.is_open();
}

/**
 * @brief Traite les retours chariots dans les données lues pour éviter les erreurs
 * @param data Les données lues contenant des retours chariots
 */
void handle_carriage_return(vector<string>& row)
{
    string last_w = row[row.size()-1];
    if (!last_w.empty() && last_w[last_w.size() - 1] == '\r')
    {
       row[row.size()-1] = last_w.erase(last_w.size() - 1);
    }
}