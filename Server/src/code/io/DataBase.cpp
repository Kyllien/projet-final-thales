#include "DataBase.hpp"

#define DB "data/dataBase.db"

/**

 *   \brief Convertit une chaîne JSON en une entrée SQL
 *   Cette fonction prend en entrée une chaîne de caractères contenant une structure JSON
 *   et l'ajoute à la base de données SQL.
 *   \param data La chaîne JSON à convertir en entrée SQL
*/
void JSONtoSQL(string data)
{
    if (data.at(0) == '{')
    {
        //cout << "Received :" << data << endl;
        nlohmann::json data_json = nlohmann::json::parse(data);
        
        //cout << "State: " << string data_json["BME280"]["State"] << endl;
        
        // INIT les objets si le statut du capteur est OK and add in SQL
        if (data_json["BME280"]["State"] == "OK")
        {
            int timestamp = data_json["Timestamp"].get<int>();
            string balise = data_json["Balise"].get<std::string>();
            int humidity = data_json["BME280"]["Humidity"].get<int>();
            int pressure = data_json["BME280"]["Pressure"].get<int>();
            int temperature = data_json["BME280"]["Temperature"].get<int>();


            BME bme(timestamp, balise, humidity, pressure, temperature);
            bme.addSQL();
            //bme.infos();
            
        }
        if (data_json["HC-SR501"]["State"] == "OK")
        {                
            HC hc(data_json["Timestamp"],data_json["Balise"],data_json["HC-SR501"]["Movement"]);
            hc.addSQL();
        }
        if (data_json["QMC5883L"]["State"] == "OK")
        {
            QMC qmc(data_json["Timestamp"],data_json["Balise"],data_json["QMC5883L"]["X"],data_json["QMC5883L"]["Y"],data_json["QMC5883L"]["Z"]);
            qmc.addSQL();
        }
        
    }
}

/**

   * @brief Obtient les données d'une table SQL et les envoie au client via un socket
   * @param sql La requête SQL à exécuter pour obtenir les données de la table
   * @param sock Le socket utilisé pour envoyer les données au client
    */
void Get_Table2(const char *sql, int sock)
{ 
    sqlite3 *db;
    sqlite3_stmt *stmt;

    // Connect to the database
    sqlite3_open(DB, &db);

    // Prepare the statement
    sqlite3_prepare_v2(db, sql, -1, &stmt, nullptr);
    
    while (sqlite3_step(stmt) == SQLITE_ROW) 
    {
        string data = SQLtoJSON2(stmt);
        ssize_t sent_len = send(sock, data.c_str(), data.size(), 0);
            if (sent_len < 0) {
                std::cerr << "Error sending data to client" << std::endl;
                break;
            }
    }
    string FIN = "ERROR73";
    ssize_t sent_len = send(sock, FIN.c_str(), FIN.size(), 0);
    if (sent_len < 0) {
        std::cerr << "Error sending data to client" << std::endl;
    }
    // Clean up
    sqlite3_finalize(stmt);
    sqlite3_close(db);
    sleep(1);
}

/**

   * @brief Convertit un enregistrement SQL en chaîne JSON
   * @param stmt Le statement SQLite3 contenant l'enregistrement à convertir en JSON
   * @return La chaîne JSON convertie à partir de l'enregistrement SQL
    */


string SQLtoJSON2(sqlite3_stmt *stmt)
{
    nlohmann::json row;
    int columns = sqlite3_column_count(stmt);
    for (int i = 0; i < columns; i++) 
    {
        switch (sqlite3_column_type(stmt, i)) 
        {
                case SQLITE_INTEGER:
                    row[sqlite3_column_name(stmt, i)] = sqlite3_column_int(stmt, i);
                    break;
                case SQLITE_FLOAT:
                    row[sqlite3_column_name(stmt, i)] = sqlite3_column_double(stmt, i);
                    break;
                case SQLITE_TEXT:
                    const unsigned char* data = sqlite3_column_text(stmt, i);
                    string data_string(data, data + sizeof(data));
                    row[sqlite3_column_name(stmt, i)] = data_string;
                    break;
        }
    }
    return row.dump();
}