/** 
 * @file TCP.cpp
 * @brief Programme Serveur
*/

#include "TCP.hpp"

using namespace std;


/**
 * @brief Initialise la socket du serveur
 * @param port numéro de port 
 * @return le numéro de la socket créée si succès, 0 si échec de la création de la socket, -1 si échec de la liaison de la socket, -2 si échec de la mise en écoute
 */

int Init_SocketServer(int port)
{
    // create a socket
    int listening = socket(AF_INET,SOCK_STREAM,0);
    if (listening <= 0)
    {
        return 0;
    }

    // bind the socket to a IP/Port
    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(port);
    inet_pton(AF_INET,"57.128.34.230",&hint.sin_addr);

    if(bind(listening, (sockaddr*)&hint, sizeof(hint)) == -1)
    {
        return -1;
    }

    // Mark the socket for listening in
    if(listen(listening, SOMAXCONN) == -1)
    {
        return -2;
    }

    return listening;
}

/**
 * @brief Initialise une socket client
 * @param listening numéro de port 
 * @return le numéro de la socket client si succès, -3 si échec de la création de la socket client
 */
int Init_SocketClient(int listening)
{
    // Accept a call 
    sockaddr_in client; 
    socklen_t clientSize = sizeof(client);
    char host[NI_MAXHOST];
    char svc[NI_MAXSERV];
    int clientSocket = accept(listening, (sockaddr*)&client, &clientSize);

    if(clientSocket == -1)
    {
        return -3;
    }

    memset(host,0,NI_MAXHOST);
    memset(svc,0, NI_MAXSERV);

    int result = getnameinfo((sockaddr*)&client, sizeof(client),host,NI_MAXHOST,svc, NI_MAXSERV,0);

    if(result)
    {
        cout << host << " connected on " << svc << endl ;
    }
    else
    {
        inet_ntop(AF_INET,&client.sin_addr,host, NI_MAXHOST);
        cout << host << " connected on " << ntohs( client.sin_port) << endl;
    }
    return clientSocket;
}

/**
 * @brief Reçoit les données envoyées par le client
 * @param clientSocket numéro de la socket client
 */
void Reception_Data(int clientSocket)
{
    string data;
    while(true)
    {
        char buffer[4096];
        // clear the buffer
        memset(buffer, 0,4096);

        // wait for a message

        int bytesRecv = recv(clientSocket, buffer, 4096,0);
        if (bytesRecv == -1 )
        {
            cerr << "There was a connection issue " << endl;
            break;
        }
        if ((bytesRecv) == 0)
        {
            cout << " The client disconnected " << endl;
            break;      
        }
        else
        {
            data = string(buffer,0,bytesRecv); //Get the data
            cout << data << endl;
        }  
        JSONtoSQL(data);
    }
    close(clientSocket);
}

/**
 * @brief Reçoit les requêtes envoyées par le client
 * @param clientSocket numéro de la socket client
 * @return La requête SQL envoyée par le client, "0" si échec de la réception de la requête
 */
string Reception_Requete(int clientSocket)
{
    char buffer[16384];
    // clear the buffer
    memset(buffer, 0,16384);

    // wait for a message
    string requete;
    int bytesRecv = recv(clientSocket, buffer, 16384,0);
    if (bytesRecv == -1 )
    {
        return  "0";
    }
    if ((bytesRecv) == 0)
    {
        return "0" ;    
    }
    else
    {
        requete = string(buffer,0,bytesRecv); //Recuperer la requete SQL provenant du client
        cout << requete << endl;
    }        
    return requete; 
}





