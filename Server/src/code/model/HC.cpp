/**
 * @file HC.cpp
 * @brief classe H
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#include "HC.hpp"
#define DB "data/dataBase.db"

/**
 * @brief Construct a new HC::HC object
 * 
 * @param timestamp timestamp de la mesure
 * @param balise nom de la balise où se trouve le capteur
 * @param movement indique si il y a du mouvement
 */
HC::HC(int timestamp, string balise, int movement)
{
    this->timestamp=timestamp;
    this->balise=balise;
    this->movement=movement;
}

/**
 * @brief Destroy the HC::HC object
 * 
 */
HC::~HC()
{
    
    //dtor
}

/**
 * @brief Fonction pour afficher les informations de la mesure
 * 
 */
void HC::infos()
{
    cout  << endl << "Timestamp " << this->Gettimestamp();
    cout  << endl << "Balise " << this->Getbalise();
    cout  << endl << "Movement " << this->Getmovement() << endl;
}
/**
 * @brief Fonction pour ajouter les informations de la mesure à la base de données
 */
void HC::addSQL()
{
    
    sqlite3 *db =NULL;
    
    int rc;
    rc = sqlite3_open(DB, &db);

    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        cout<<endl<< "MonException open :" <<endl;
    }

    sqlite3_stmt *stmt;

    const char *req = "insert into HC(timestamp, balise, movement) values (?,?,?)";


    // preparer la requete
    rc = sqlite3_prepare_v2(db,req,-1, &stmt,nullptr);
    
    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        cout<<endl<< "MonException requete :" <<endl;
    }

    // valoriser les champs connus (niveau Contact)
    sqlite3_bind_int(stmt,1,this->Gettimestamp());
    sqlite3_bind_text(stmt,2,this->Getbalise(),strlen(this->Getbalise()),NULL);
    sqlite3_bind_int(stmt,3,this->Getmovement());
    

    rc = sqlite3_step(stmt);
    printf("%s: %s\n", sqlite3_errstr(sqlite3_extended_errcode(db)), sqlite3_errmsg(db));
    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        // cout<<endl<< "MonException step :" <<endl;
    }

    rc = sqlite3_finalize(stmt);

    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        // cout<<endl<< "MonException finalize:" <<endl;
    }

    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        // cout<<endl<< "MonException fermeture:" <<endl;
    }

}
