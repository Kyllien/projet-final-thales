/**
 * @file QMC.cpp
 * @brief classe QMC
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#include "QMC.hpp"

#define DB "data/dataBase.db"

/**
 * @brief Construct a new QMC::QMC object
 * 
 * @param timestamp timestamp de la mesure
 * @param balise nom de la balise où se trouve le capteur
 * @param x 1ere valeur de la position
 * @param y 2eme valeur de la position
 * @param z 3eme valeur de la position
 */
QMC::QMC(int timestamp, string balise, int x,int y, int z)
{
    this->timestamp=timestamp;
    this->balise=balise;
    this->x=x;
    this->z=z;
    this->y=y;
}
/**
 * @brief Destroy the QMC::QMC object
 * 
 */
QMC::~QMC()
{
    
    //dtor
}

/**
 * @brief Fonction pour afficher les informations de la mesure
 * 
 */
void QMC::infos()
{
    cout  << endl << "Timestamp " << this->Gettimestamp();
    cout  << endl << "Balise " << this->Getbalise();
    cout  << endl << "x " << this->Getx();
    cout  << endl << "y " << this->Gety();
    cout  << endl << "z " << this->Getz() << endl;
}
/**
 * @brief Fonction pour ajouter les informations de la mesure à la base de données
 */
void QMC::addSQL()
{
    
    sqlite3 *db =NULL;
    
    int rc;
    rc = sqlite3_open(DB, &db);

    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        cout<<endl<< "MonException open :" <<endl;
    }

    sqlite3_stmt *stmt;

    const char *req = "insert into QMC(timestamp, balise, x, y, z) values (?,?,?,?,?)";


    // preparer la requete
    rc = sqlite3_prepare_v2(db,req,-1, &stmt,nullptr);
    
    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        cout<<endl<< "MonException requete :" <<endl;
    }

    // valoriser les champs connus (niveau Contact)
    sqlite3_bind_int(stmt,1,this->Gettimestamp());
    sqlite3_bind_text(stmt,2,this->Getbalise(),strlen(this->Getbalise()),NULL);
    sqlite3_bind_int(stmt,3,this->Getx());
    sqlite3_bind_int(stmt,4,this->Gety());
    sqlite3_bind_int(stmt,5,this->Getz());

    rc = sqlite3_step(stmt);
    printf("%s: %s\n", sqlite3_errstr(sqlite3_extended_errcode(db)), sqlite3_errmsg(db));
    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        // cout<<endl<< "MonException step :" <<endl;
    }

    rc = sqlite3_finalize(stmt);

    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        // cout<<endl<< "MonException finalize:" <<endl;
    }

    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        // cout<<endl<< "MonException fermeture:" <<endl;
    }

}
