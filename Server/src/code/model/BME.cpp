#include "BME.hpp"
/**
 * @brief Constructeur de la classe BME
 * @param timestamp timestamp de la mesure
 * @param balise nom de la balise où se trouve le capteur
 * @param humidity taux d'humidité mesuré
 * @param pressure pression atmosphérique mesurée
 * @param temperature température mesurée
 */
BME::BME(int timestamp, string balise,int humidity,int pressure, int temperature)
{
    this->timestamp=timestamp;
    this->balise=balise;
    this->pressure=pressure;
    this->humidity=humidity;
    this->temperature=temperature;
}
/**
 * @brief Destructeur de la classe BME
 */
BME::~BME()
{
    
    //dtor
}

/**
 * @brief Fonction pour afficher les informations de la mesure
 */
void BME::infos()
{
    cout  << endl << "Timestamp " << this->Gettimestamp();
    cout  << endl << "Balise " << this->Getbalise();
    cout  << endl << "Pressure " << this->Getpressure();
    cout  << endl << "Temperature " << this->Gettemperature();
    cout  << endl << "Humidity " << this->Gethumidity() << endl;
}

/**
 * @brief Fonction pour ajouter les informations de la mesure à la base de données
 */
void BME::addSQL()
{
    
    sqlite3 *db =NULL;
    
    int rc;
    rc = sqlite3_open(DB, &db);

    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        cout<<endl<< "MonException open :" <<endl;
    }

    sqlite3_stmt *stmt;

    const char *req = "insert into BME(timestamp, balise, humidity, pressure, temperature) values (?,?,?,?,?)";


    // preparer la requete
    rc = sqlite3_prepare_v2(db,req,-1, &stmt,nullptr);
    
    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        cout<<endl<< "MonException requete :" <<endl;
    }

    // valoriser les champs connus (niveau Contact)
    sqlite3_bind_int(stmt,1,this->Gettimestamp());
    sqlite3_bind_text(stmt,2,this->Getbalise(),strlen(this->Getbalise()),NULL);
    sqlite3_bind_int(stmt,3,this->Gethumidity());
    sqlite3_bind_int(stmt,4,this->Getpressure());
    sqlite3_bind_int(stmt,5,this->Gettemperature());

    rc = sqlite3_step(stmt);
    printf("%s: %s\n", sqlite3_errstr(sqlite3_extended_errcode(db)), sqlite3_errmsg(db));
    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        //cout<<endl<< "MonException step :" <<endl;
    }

    rc = sqlite3_finalize(stmt);

    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        //cout<<endl<< "MonException finalize:" <<endl;
    }

    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        // Exception si ouverrture not ok
        //cout<<endl<< "MonException fermeture:" <<endl;
    }

}

