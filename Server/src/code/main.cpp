/**
 * @file main.cpp
 * @brief fonction main du programme serveur
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#include "app.hpp"

/**
 * @brief Exécute le programme serveur 
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char* argv[])
{
    app app;
    if(app.init(argc, argv))
    {
        app.run();
    }


}