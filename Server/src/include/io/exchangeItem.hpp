/**
* @file exchangeItem.hpp
* @brief Classe pour stocker les données d'échange
*/

#pragma once
#include "common.hpp"
#include <vector>
#include <map>


class ExchangeItem
{
    public:
        /**
         * @brief Constructeur de la classe ExchangeItem
         * @param properties vecteur de string contenant les noms des propriétés à stocker
         */
        ExchangeItem(){};
        ExchangeItem(const vector<string>& properties);

        /**
         * @brief Fonction qui permet de définir les propriétés de l'objet
         * @param properties vecteur de string contenant les noms des propriétés à stocker
         */
        void setProperties(const vector<string>& properties);
        /**
         * @brief Fonction qui retourne les valeurs des propriétés sous forme de vecteur de string
         * @return vecteur de string contenant les valeurs des propriétés
         */
        vector<string> getValues();
        /**
         * @brief Surcharge de l'opérateur[] pour accéder à la valeur d'une propriété en utilisant son nom en tant que clé
         * @param key nom de la propriété
         * @return référence vers la valeur de la propriété
         * @exception out_of_range si la clé passée en paramètre n'existe pas
         */
        string& operator[](string key);
    private:
        map<string,string> data;
        vector<string> properties;
};