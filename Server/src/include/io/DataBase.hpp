#ifndef DATABASE_HPP_INCLUDED
#define DATABASE_HPP_INCLUDED

#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <nlohmann/json.hpp>
#include <sqlite3.h>
#include <mutex>
#include <thread>

#include "BME.hpp"
#include "HC.hpp"
#include "QMC.hpp"


using namespace std;


void Get_Table2(const char *, int);
string SQLtoJSON2(sqlite3_stmt *);
void JSONtoSQL(string);


#endif //DATABASE_HPP_INCLUDED