#ifndef TCP_HPP_INCLUDED
#define TCP_HPP_INCLUDED

#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <nlohmann/json.hpp>
#include <sqlite3.h>
#include <mutex>
#include <thread>

#include "BME.hpp"
#include "HC.hpp"
#include "QMC.hpp"
#include "DataBase.hpp"

using namespace std;


int Init_SocketClient(int);
string Reception_Requete(int);
int Init_SocketServer(int);
void Reception_Data(int);



#endif //TCP_HPP_INCLUDED