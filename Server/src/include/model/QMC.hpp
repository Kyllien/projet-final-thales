/**
 * @file QMC.hpp
 * @brief Classe QMC
 * @copyright Copyright (c) 2023
 * 
 */
#ifndef QMC_HPP
#define QMC_HPP



#include<iostream>
#include <sstream>
#include<stdlib.h>
#include <sqlite3.h>
#include <cstring>

#define DB "data/dataBase.db"

using namespace std;


/**
 * @class QMC
 * @brief Classe représentant les données d'un capteur QMC
 * 
 */
class QMC
{
    public:
        QMC(int, string, int, int, int);
        virtual ~QMC();

        /**
         * @brief Fonction pour récupérer le timestamp de la mesure
         * @return timestamp de la mesure
         */
        int Gettimestamp() { return timestamp; }

        /**
         * @brief Fonction pour modifier le timestamp
         */
        void Settimestamp(int val) { timestamp = val; }

        /**
         * @brief Fonction pour récupérer le nom de la balise
         * @return nom de la balise
         */
        char* Getbalise() { return &balise[0]; }

        /**
         * @brief Fonction pour modifier la balise
         */
        void Setbalise(string val) { balise = val; }

        /**
         * @brief Fonction pour récupérer la 1ere valeur de la position
         * @return 1ere valeur de la position
         */
        int Getx() { return x; }
        /**
         * @brief Fonction pour modifier la 1ere valeur de la position
         */
        void Setx(int val) { x = val; }
        /**
         * @brief Fonction pour récupérer la 1eme valeur de la position
         * @return 2eme valeur de la position
         */
        int Gety() { return y; }
        /**
         * @brief Fonction pour modifier la 2eme valeur de la position
         */
        void Sety(int val) { y = val; }
        /**
         * @brief Fonction pour récupérer la 3eme valeur de la position
         * @return 3eme valeur de la position
         */
        int Getz() { return z; }
        /**
         * @brief Fonction pour modifier la 3eme valeur de la position
         */
        void Setz(int val) { z = val; }

        void addSQL();

        void infos();

    protected:

    private:
        int timestamp;/**< timestamp de la mesure */
        string balise;/**< nom de la balise où se trouve le capteur */
        int x;/**< 1ere valeur de la position */
        int y;/**< 2eme valeur de la position */
        int z;/**< 3eme valeur de la position */
};

#endif // QMC_HPP
