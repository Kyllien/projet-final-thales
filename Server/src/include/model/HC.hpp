/**
 * @file HC.hpp
 * @brief Classe HC
 * @copyright Copyright (c) 2023
 * 
 */
#ifndef HC_HPP
#define HC_HPP

#include<iostream>
#include <sstream>
#include<stdlib.h>
#include <sqlite3.h>
#include <cstring>

#define DB "data/dataBase.db"

using namespace std;


/**
 * @brief Classe représentant les d'un capteur HC
 * @class HC
 * 
 */
class HC
{
    public:
        HC(int, string, int);
        virtual ~HC();
        /**
         * @brief Fonction pour récupérer le timestamp de la mesure
         * @return timestamp de la mesure
         */
        int Gettimestamp() { return timestamp; }
        /**
         * @brief Fonction pour modifier le timestamp
         */
        void Settimestamp(int val) { timestamp = val; }
        /**
         * @brief Fonction pour récupérer le nom de la balise
         * @return nom de la balise
         */
        char* Getbalise() { return &balise[0]; }
        /**
         * @brief Fonction pour modifier la balise
         */
        void Setbalise(string val) { balise = val; }

        /**
         * @brief Fonction pour récupérer le mouvement
         * @return la valeur du mouvement 
         */
        int Getmovement() { return movement; }

        /**
         * @brief Fonction pour modifier le mouvement
         */
        void Setmovement(int val) { movement = val; }

        void addSQL();

        void infos();

    protected:

    private:
        int timestamp;/**< timestamp de la mesure */
        string balise;/**< nom de la balise où se trouve le capteur */
        int movement; /**< indique si il y a une détection de mouvement*/
};

#endif // HC_HPP
