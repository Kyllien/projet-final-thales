/**
 * @file BME.hpp
 * @brief Fichier contenant la déclaration de la classe BME
 */

#ifndef BME_HPP
#define BME_HPP

#include<iostream>
#include <sstream>
#include<stdlib.h>
#include <sqlite3.h>
#include <cstring>

#define DB "data/dataBase.db"

using namespace std;


/**
 * @class BME
 * @brief Classe représentant les données d'un capteur BME
 */
class BME
{
    public:
        
        BME(int, string, int, int, int);
        virtual ~BME();
        /**
         * @brief Fonction pour récupérer le timestamp de la mesure
         * @return timestamp de la mesure
         */
        int Gettimestamp() { return timestamp; }
        
        /**
         * @brief Fonction pour modifier le timestamp
         */
        void Settimestamp(int val) { timestamp = val; }

        /**
         * @brief Fonction pour récupérer le nom de la balise
         * @return nom de la balise
         */
        char* Getbalise() { return &balise[0]; }

        /**
         * @brief Fonction pour modifier la balise
         */
        void Setbalise(string val) { balise = val; }
        /**
         * @brief Fonction pour récupérer la pression atmosphérique mesurée
         * @return pression atmosphérique mesurée
         */
        int Getpressure() { return pressure; }

        /**
         * @brief Fonction pour modifier la pression atmosphérique 
         */
        void Setpressure(int val) { pressure = val; }
        /**
         * @brief Fonction pour récupérer le taux d'humidité mesuré
         * @return taux d'humidité mesuré
         */
        int Gethumidity() { return humidity; }
        /**
         * @brief Fonction pour modifier l'humidité
         */
        void Sethumidity(int val) { humidity = val; }
        /**
         * @brief Fonction pour récupérer la température mesurée
         * @return température mesurée
         */
        int Gettemperature() { return temperature; }
        /**
         * @brief Fonction pour modifier la température
         */
        void Settemperature(int val) { temperature = val; }

        void addSQL();

        void infos();

    protected:

    private:
        int timestamp; /**< timestamp de la mesure */
        string balise; /**< nom de la balise où se trouve le capteur */
        int pressure; /**< taux d'humidité mesuré */
        int humidity; /**< pression atmosphérique mesurée */
        int temperature; /**< température mesurée */
};

#endif // BME_HPP
