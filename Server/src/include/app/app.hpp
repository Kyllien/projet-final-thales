/**
 * @file app.hpp
 * @brief fichier contenant la classe app
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#pragma once
#include "common.hpp"
#include "log.hpp"

#include "GestionTache.hpp"


using namespace std;


/**
 * @class app 
 * @brief classe permettant d'initialiser et d'exécuter le programme du serveur
 */
class app
{
    public:
        /**
         * @brief Construct a new app object
         * 
         */
        app(){}

        /**
         * @brief Destroy the app object
         * 
         */
        ~app(){this->clean();}
        
        bool init(int argc, char* argv[]);
        void run();
    private:
        void clean();
        void exit();
};