/**
 * @file GestionTache.hpp
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#pragma once

#include <iostream>
#include <pthread.h>
#include <fstream>
#include <thread>
#include <string.h>
#include <mutex>
#include <string.h>
#include <sqlite3.h>

#include "TCP.hpp"
#include "DataBase.hpp"


// #include "RaspberryMaitre.hpp"
// #include "Client2.hpp"

using namespace std;

void InitTache();
